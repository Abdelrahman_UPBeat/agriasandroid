package com.nav.appiqo.agriyas.views.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.AddToCartModel;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.CuttingTypeModel;
import com.nav.appiqo.agriyas.adapter.CartAdapter;
import com.nav.appiqo.agriyas.adapter.CuttingItemAdapter;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.shts.android.storiesprogressview.StoriesProgressView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDescriptionFragment extends Fragment implements StoriesProgressView.StoriesListener {


    @BindView(R.id.iv_adddescp)
    LinearLayout ivAdddescp;
    @BindView(R.id.tv_itemaddminusescp)
    TextView tvItemaddminusescp;
    @BindView(R.id.iv_minusdescp)
    LinearLayout ivMinusdescp;
    @BindView(R.id.tv_order)
    TextView tvOrder;
    @BindView(R.id.mainlayoutdescp)
    ScrollView mainlayoutdescp;
    @BindView(R.id.tv_itemname)
    TextView tvItemname;
    @BindView(R.id.tv_itempriceDescp)
    TextView tvItempriceDescp;
    @BindView(R.id.tv_itemDescription)
    TextView tvItemDescription;
    @BindView(R.id.llayout)
    LinearLayout llayout;
    @BindView(R.id.id_viewred)
    View idViewred;
    @BindView(R.id.iv_itemdescp)
    ImageView ivItemdescp;
    @BindView(R.id.iv_toolbar_desc_back)
    ImageView ivToolbarDescBack;
    @BindView(R.id.tv_toolbar_desc_title)
    TextView tvToolbarDescTitle;
   /* @BindView(R.id.itemdescp_viewpar)
    ViewPager itemdescpViewpar;*/

    String descp = "", itemname = "", itemprice = "", itemid = "", maxQuantity = "";
    public static String typeOfCutting = "";
    int[] mResources;
    ArrayList<String> images;

    EditText etspecification;
    RecyclerView rv_Cutting;
    RadioGroup radioGroup;
    RadioButton rb;

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    String s = "1";
    String t = "1";
    String sAdd = "";
    int i = 1;
    Dialog dialog1;


    List<AddToCartModel.DataBean> dataBeans;
    List<CuttingTypeModel.DataBeanCutting> dataBeansCuttingType;

    View viewFragment, viewDialog;

    Unbinder unbinder;
    @BindView(R.id.tv_amounttopay)
    TextView tvAmounttopay;

    CountDownTimer countDownTimer;

    CardView amttopaylayout;
    Animation animSlideup;

    StoriesProgressView storiesProgressView;
    @BindView(R.id.tv_weight)
    TextView tvWeight;

    // CustomPagerAdapter mCustomPagerAdapter;
    int counter = 0;
    @BindView(R.id.stories)
    StoriesProgressView stories;
    @BindView(R.id.reverse)
    View reverse;
    @BindView(R.id.skip)
    View skip;
    private CuttingItemAdapter cuttingItemAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewFragment = inflater.inflate(R.layout.fragment_item_descp, container, false);
        unbinder = ButterKnife.bind(this, viewFragment);
        typeOfCutting = "";
        progressView = new ProgressView(getActivity());
        apiInterface = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        prefData = new PrefData(getActivity());
        dataBeans = new ArrayList<>();
        dataBeansCuttingType = new ArrayList<>();
        images = new ArrayList<>();
        amttopaylayout = viewFragment.findViewById(R.id.amttopaylayout);


        animSlideup = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);
        animSlideup.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



       /* mCustomPagerAdapter = new CustomPagerAdapter(getActivity());

        itemdescpViewpar = viewFragment.findViewById(R.id.itemdescp_viewpar);
        itemdescpViewpar.setAdapter(mCustomPagerAdapter);*/

        ivToolbarDescBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   countDownTimer.cancel();
                getActivity().onBackPressed();
            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            descp = bundle.getString("decription");
            itemname = bundle.getString("itemname");
            itemprice = bundle.getString("itemprice");
            itemid = bundle.getString("itemid");
            images = bundle.getStringArrayList("images");
            maxQuantity = bundle.getString("maxQuantity");
            Log.i("tag465465", "onCreateView: " + images);
        }

        tvToolbarDescTitle.setText(itemname);
        tvItemDescription.setText(descp);
        tvItemname.setText(itemname);
        tvItempriceDescp.setText(itemprice);


        storiesProgressView = viewFragment.findViewById(R.id.stories);
      //  storiesProgressView.setStoriesCount(images.size());
      //  storiesProgressView.setStoryDuration(3000L);
     //   storiesProgressView.setStoriesListener(this);
    //    storiesProgressView.startStories();

       /* skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.skip();
            }
        });
        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storiesProgressView.reverse();
            }
        });*/
        Glide.with(ItemDescriptionFragment.this).load(images.get(counter)).into(ivItemdescp);

        ivMinusdescp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")) {
//                    Utils.showSnackBar(mainlayoutdescp, getString(R.string.login_to_order), getActivity());
//
//                } else {

                    if (Integer.parseInt(maxQuantity) < 1) {
                        Utils.showSnackBar(mainlayoutdescp, getString(R.string.out_of_stock), getActivity());
                    } else if (i < Integer.parseInt(maxQuantity)) {


                        try{
                            i++;
                            s = Integer.toString(i);
                            int iadd = Integer.parseInt(itemprice) * i;
                            tvItemaddminusescp.setText(s + " ");
                            sAdd = Integer.toString(iadd);
                        }catch (Exception e){}
                        try{
                            // amttopaylayout.setVisibility(View.VISIBLE);
                            amttopaylayout.startAnimation(animSlideup);
                            tvAmounttopay.setText(getResources().getString(R.string.amount_to_pay) + sAdd);
                        }catch (Exception e){

                        }

/*
                        countDownTimer = new CountDownTimer(3000, 2000) {
                            public void onTick(long millisUntilFinished) {
                                try{
                                   // amttopaylayout.setVisibility(View.VISIBLE);
                                    amttopaylayout.startAnimation(animSlideup);
                                    tvAmounttopay.setText(getResources().getString(R.string.amount_to_pay) + sAdd);
                                }catch (Exception e){

                                }

                            }

                            public void onFinish() {
                                try{
                                    amttopaylayout.setVisibility(View.INVISIBLE);
                                }catch (Exception e){}

                            }
                        }.start();
*/


                        // Toast.makeText(getActivity(), "Amount To Pay" + sAdd, Toast.LENGTH_SHORT).show();
                    } else {
                        try{

                            Utils.showSnackBar(mainlayoutdescp, getString(R.string.maximum_purchase_quantity) + i, getActivity());
                        }catch (Exception e){}
                    }
                }
//            }
        });

        ivAdddescp.setOnClickListener(view -> {

//            if (ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")) {
////                Utils.showSnackBar(mainlayoutdescp, getString(R.string.login_to_order), getActivity());
////            } else {
                if (i > 1) {
                    i--;
                    t = Integer.toString(i);
                    int kamountsow = i * Integer.parseInt(itemprice);
                    final String minusamount = Integer.toString(kamountsow);
                    tvItemaddminusescp.setText(t + " "+getResources().getString(R.string.kg));

//   amttopaylayout.setVisibility(View.VISIBLE);
                    amttopaylayout.startAnimation(animSlideup);
                    tvAmounttopay.setText(getString(R.string.amount_to_pay) + minusamount);
/*
                    countDownTimer = new CountDownTimer(3000, 2000) {
                        public void onTick(long millisUntilFinished) {
                         //   amttopaylayout.setVisibility(View.VISIBLE);
                            amttopaylayout.startAnimation(animSlideup);
                            tvAmounttopay.setText(getString(R.string.amount_to_pay) + minusamount);
                        }

                        public void onFinish() {
                            amttopaylayout.setVisibility(View.INVISIBLE);
                        }
                    }.start();
*/

                    //Toast.makeText(getActivity(), "Amount To Pay" + minusamount, Toast.LENGTH_SHORT).show();
                } else {
                    Utils.showSnackBar(mainlayoutdescp, getString(R.string.maximum_purchase_quantity) + i, getActivity());
                }
//            }
        });
        connectApiGetCutting();

        tvOrder.setOnClickListener(view -> {
            int stock = Integer.parseInt(maxQuantity);
            if (stock < 1) {
                Utils.showSnackBar(mainlayoutdescp, getString(R.string.out_of_stock), getActivity());
                // TODO: 9/19/2018 if user is guest
            } /*else if (ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")) {
                // Utils.showSnackBar(mainlayoutdescp, "Please Login to continue order", getActivity());
                Toast.makeText(getActivity(), getString(R.string.login_to_order), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            } */else {
                if (dataBeansCuttingType.size()==0){
                    typeOfCutting = "";
                    connectApiAddToCart();
                }else {
                    showDialog();


                }
            }
        });

        return viewFragment;
    }

    public void showDialog() {
        dialog1 = new Dialog(getActivity(), R.style.DialogFragmentTheme);
        viewDialog = getLayoutInflater().inflate(R.layout.typeofcuttingdialoscreen, null);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setCanceledOnTouchOutside(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(viewDialog);

        viewDialog.clearFocus();
        viewDialog.setOnClickListener(v -> dialog1.dismiss());

        final RadioButton r1, r2, r3;
        TextView btnOk;
        btnOk = viewDialog.findViewById(R.id.tv_cutting_ok);
        etspecification = viewDialog.findViewById(R.id.et_oterspecification);
        rv_Cutting = viewDialog.findViewById(R.id.rv_Cutting);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_Cutting.setLayoutManager(linearLayoutManager);
        cuttingItemAdapter = new CuttingItemAdapter(getActivity(), dataBeansCuttingType);
        rv_Cutting.setAdapter(cuttingItemAdapter);

        radioGroup = viewDialog.findViewById(R.id.radiogrp);
        radioGroup.clearCheck();
        r1 = viewDialog.findViewById(R.id.cuttin1);
        r2 = viewDialog.findViewById(R.id.cuttin2);
        r3 = viewDialog.findViewById(R.id.cuttin3);
        viewDialog.setFocusable(false);
        viewDialog.setFocusableInTouchMode(false);
        radioGroup.setFocusable(false);
        radioGroup.setFocusableInTouchMode(false);

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            rb = group.findViewById(checkedId);

            switch (checkedId) {
                case R.id.cuttin1:
                    typeOfCutting = r1.getText().toString();
                    break;
                case R.id.cuttin2:
                    typeOfCutting = r2.getText().toString();
                    break;
                case R.id.cuttin3:
                    typeOfCutting = r3.getText().toString();
                    break;
            }
        });

        btnOk.setOnClickListener(v -> {
            if (typeOfCutting.equalsIgnoreCase("")) {
                Utils.showSnackBar(viewDialog, getString(R.string.select_cutting_type), getActivity());
            } else {
                connectApiAddToCart();
            }
        });

        dialog1.show();

    }
    private void connectApiGetCutting() {
        progressView.showLoader();
        String userId  ;
        if(ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")){
            userId = "1";
        }else{
            userId = MyApplication.readStringPref(PrefData.USER_ID);
        }

        String type = "add";

        Call<CuttingTypeModel> call = apiInterface.getCuttingType(userId, itemid, type, Util.getLocale());
        call.enqueue(new Callback<CuttingTypeModel>() {
            @Override
            public void onResponse(Call<CuttingTypeModel> call, Response<CuttingTypeModel> response) {
                progressView.hideLoader();
                try{
                    if (!response.body().isError()) {

                        dataBeansCuttingType.clear();
                        dataBeansCuttingType.addAll(response.body().getData());
                      //  typeOfCutting = dataBeansCuttingType.get(0).getType();



                    } else {

                        dialog1.dismiss();
                    }

                }catch (Exception e){

                }


            }

            @Override
            public void onFailure(Call<CuttingTypeModel> call, Throwable t) {
                t.printStackTrace();
                progressView.hideLoader();
            }
        });
    }

    private void connectApiAddToCart() {
        progressView.showLoader();
        String userId  ;
        if(ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")){
            userId = MyApplication.readStringPref(PrefData.DEVICE_ID);
        }else{
            userId = MyApplication.readStringPref(PrefData.USER_ID);
        }


        String quantity = tvItemaddminusescp.getText().toString().replace("Kg", "").replace(" ", "");
        String type = "add";
        String CuttingType = typeOfCutting;
        MyApplication.writeStringPref(PrefData.cutting_type, CuttingType);
        String otherSpecification ="";
        try{
             otherSpecification = etspecification.getText().toString().trim();

        }catch (Exception e){
            otherSpecification ="";
        }

        Call<AddToCartModel> call = apiInterface.addToCart(userId, itemid, quantity, itemprice, type, CuttingType, otherSpecification, Util.getLocale());
        call.enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                progressView.hideLoader();

                if (!response.body().isError()) {

                    dataBeans.clear();
                    dataBeans.addAll(response.body().getData());
                    try{
                        dialog1.dismiss();

                    }catch (Exception e){}


                    Fragment_Cart fragCart = new Fragment_Cart();
                    Bundle bundle = new Bundle();
                    bundle.putString("cart", "fromItemDescription");
                    fragCart.setArguments(bundle);

                    ((ControllerActivity) getActivity()).displayCaned(fragCart, "fragCart");
                } else {
                    Util.showSnackBar(mainlayoutdescp, response.body().getMessage(), getActivity());
                    dialog1.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                t.printStackTrace();
                progressView.hideLoader();
            }
        });
    }

    @Override
    public void onNext() {
        Glide.with(ItemDescriptionFragment.this).load(images.get(++counter)).into(ivItemdescp);

    }

    @Override
    public void onPrev() {
        Glide.with(ItemDescriptionFragment.this).load(images.get(--counter)).into(ivItemdescp);
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //storiesProgressView.destroy();
        unbinder.unbind();
    }

    public class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.itemdecpimageitem, container, false);

            ImageView imageView = itemView.findViewById(R.id.iv_itemdescp);
            Glide.with(ItemDescriptionFragment.this).load(images.get(0)).into(ivItemdescp);


            // imageView.setImageResource(mResources[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}



