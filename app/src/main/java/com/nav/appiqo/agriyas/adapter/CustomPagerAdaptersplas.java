package com.nav.appiqo.agriyas.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nav.appiqo.agriyas.models.Splasviewpamodel;

 public class CustomPagerAdaptersplas extends PagerAdapter {

    private Context mContext;

    public CustomPagerAdaptersplas(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        Splasviewpamodel modelObject = Splasviewpamodel.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return Splasviewpamodel.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Splasviewpamodel customPagerEnum = Splasviewpamodel.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }}