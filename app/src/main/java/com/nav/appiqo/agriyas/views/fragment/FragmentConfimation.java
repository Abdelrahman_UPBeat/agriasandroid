package com.nav.appiqo.agriyas.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentConfimation extends Fragment {
    Toolbar toolbar;


    String orderId = "", purchaseAmount = "";
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_order_amount)
    TextView tvOrderAmount;
    @BindView(R.id.btn_orderhistory)
    Button btnOrderhistory;
    @BindView(R.id.tv_tocart)
    TextView tvTocart;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_confirmation, container, false);
        unbinder = ButterKnife.bind(this, v);
        toolbar = v.findViewById(R.id.toolbarconfirm);
        //getActivity().setSupportActionBar(toolbar);
        //  hideTool();
        ((ControllerActivity)getActivity()).disableDrwable();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //orderId = getActivity().getIntent().getStringExtra("orderId");
            //  purchaseAmount = getActivity().getIntent().getStringExtra("purchaseAmount");
            orderId = bundle.getString("orderId");
            purchaseAmount = bundle.getString("purchaseAmount");
        }

        tvOrderId.setText(orderId);
        tvOrderAmount.setText(purchaseAmount);

        toolbar.setVisibility(View.VISIBLE);

        btnOrderhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ControllerActivity) getActivity()).displayCaned(new Fragment_Orders(), "order");
            }
        });

        tvTocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ControllerActivity) getActivity()).displayCaned(new Fragment_Cart(), "cart");

            }
        });

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}





