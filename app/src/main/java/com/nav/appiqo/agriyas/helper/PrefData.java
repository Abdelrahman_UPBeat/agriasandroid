package com.nav.appiqo.agriyas.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hp on 4/5/2018.
 */

public class PrefData {


    public static final String DEVICE_ID = "DEVICE_ID";
    public static String PREF_LOGINSTATUS = "pref_loginstatus";


    public static String LOGINSTATUS = "LOGINSTATUS";


    public static String PREF_DEVIC_ID = "pref_deviceid";
    public static String USER_ID = "User_id";
    public static String full_name = "Full_name";
    public static String profilepic = "profile_pic";

    public static String deviceId = "deviceId";


    public static String sizeofcart = "0";
    public static String securityToken = "securityToken";


    public static String pucaseamount = "pucaseamount";

    public static String last_name = "last_name";
    public static String user_dob = "user_dob";
    public static String user_country = "user_country";
    public static String user_gender = "user_gender";
    public static String user_email = "user_email";
    public static String user_mobile = "user_mobile";
    public static String user_password = "user_password";
    public static String User_userfile = "User_userfile";
    public static String PREF_FCM_TOK = "pref_fcmtoken";
    public static String item_id = "item_id";
    public static String item_name = "item_name";
    public static String image = "http://localhost/api/mfa/api/uploads/item/slider1.jpg";
    public static String price = "price";
    public static String stock = "stock";
    public static String description = "description";
    public static String cartimageone = "cartimageone";
    public static String quantity = "quantity";
    public static String cart_id = "cart_id";
    public static String guest_login = "guestLogin";
    public static String cutting_type = " ";
    private SharedPreferences mSharedPreferences;
    private String sharedPrefName = "jaZZar";
    private SharedPreferences.Editor editor;

    public PrefData(Context con) {
        mSharedPreferences = con.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
    }

    public static String getPrefLoginstatus() {
        return PREF_LOGINSTATUS;
    }

    public static void setPrefLoginstatus(String prefLoginstatus) {
        PREF_LOGINSTATUS = prefLoginstatus;
    }

    public static String getCart_id() {
        return cart_id;
    }

    public static void setCart_id(String cart_id) {
        PrefData.cart_id = cart_id;
    }

    public String getPrefDevicId() {
        return mSharedPreferences.getString(PREF_DEVIC_ID, "");
    }

    public void setPrefDevicId(String prefDevicId) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.PREF_FCM_TOK, prefDevicId);
        editor.apply();
    }

    public String getPrefFcmTok() {
        return mSharedPreferences.getString(PREF_FCM_TOK, "");
    }

    public void setPrefFcmTok(String prefFcmTok) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.PREF_FCM_TOK, prefFcmTok);
        editor.apply();
    }

    public String getLOGINSTATUS() {
        return LOGINSTATUS;
    }

    public void setLOGINSTATUS(String LOGINSTATUS) {
        PrefData.LOGINSTATUS = LOGINSTATUS;
    }

    public String getUser_country() {
        return mSharedPreferences.getString(USER_ID, "");
    }

    public void setUser_country(String user_country) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_country, user_country);
        editor.apply();

    }

    public String getUser_id() {
        return mSharedPreferences.getString(USER_ID, "");

    }

    public void setUser_id(String user_id) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.USER_ID, user_id);
        editor.apply();

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        PrefData.image = image;
    }

    public String getUser_name() {
        return mSharedPreferences.getString(full_name, "");
    }

    public void setUser_name(String user_name) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.full_name, user_name);
        editor.apply();
    }

    public String getUser_dob() {
        return mSharedPreferences.getString(user_dob, "");
    }

    public void setUser_dob(String user_dob) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_dob, user_dob);
        editor.apply();
    }

    public String getUser_gender() {
        return mSharedPreferences.getString(user_gender, "");
    }

    public void setUser_gender(String user_gender) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_gender, user_gender);
        editor.apply();
    }

    public String getUser_email() {
        return mSharedPreferences.getString(user_email, "");
    }

    public void setUser_email(String user_email) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_email, user_email);
        editor.apply();
    }

    public String getUser_mobile() {
        return mSharedPreferences.getString(user_mobile, "");
    }

    public void setUser_mobile(String user_mobile) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_mobile, user_mobile);
        editor.apply();
    }

    public String getUser_password() {
        return mSharedPreferences.getString(user_password, "");
    }

    public void setUser_password(String user_password) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.user_password, user_password);
        editor.apply();
    }

    public String getUser_userfile() {
        return mSharedPreferences.getString(User_userfile, "");
    }

    public void setUser_userfile(String user_userfile) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.User_userfile, user_userfile);
        editor.apply();
    }

    public void clear() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        PrefData.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        PrefData.item_name = item_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        PrefData.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        PrefData.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        PrefData.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        PrefData.quantity = quantity;
    }

    public String getCartimageone() {
        return cartimageone;
    }

    public void setCartimageone(String cartimageone) {
        PrefData.cartimageone = cartimageone;
    }

    public String getProfilepic() {
        return mSharedPreferences.getString(profilepic, "");
    }

    public void setProfilepic(String profilepic) {
        editor = mSharedPreferences.edit();
        editor.putString(PrefData.profilepic, profilepic);
        editor.apply();

    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        PrefData.full_name = full_name;
    }

    public String getPucaseamount() {
        return pucaseamount;
    }

    public void setPucaseamount(String pucaseamount) {
        PrefData.pucaseamount = pucaseamount;
    }

    public String getSizeofcart() {
        return sizeofcart;
    }

    public void setSizeofcart(String sizeofcart) {
        PrefData.sizeofcart = sizeofcart;
    }


}
