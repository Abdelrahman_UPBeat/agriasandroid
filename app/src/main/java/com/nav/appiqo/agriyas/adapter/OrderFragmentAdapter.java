package com.nav.appiqo.agriyas.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.MyOrdersModel;
import com.nav.appiqo.agriyas.views.activity.OrderItemActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<MyOrdersModel.DataBean> myOrderModelListFrag;


    public OrderFragmentAdapter(Context context, List<MyOrdersModel.DataBean> myOrderModelListFrag) {
        this.context = context;
        this.myOrderModelListFrag = myOrderModelListFrag;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_rv_fralayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder holder2 = (ViewHolder) holder;

        String datetime;


        datetime = myOrderModelListFrag.get(position).getBooking_dt();
        String datetime2 = Util.parseDateToddMMyyyy(datetime);

        String date = datetime2.substring(0, 11);
        String time = datetime2.substring(11);
        holder2.tvOrderid.setText(context.getResources().getString(R.string.order_id_id)+":-" +
                myOrderModelListFrag.get(position).getOrder_id());
        holder2.tvOrderdate.setText(context.getResources().getString(R.string.date)+":-" + date);
        holder2.tvOrdertime.setText(context.getResources().getString(R.string.time)+":-" + time);
        holder2.tvPriceorderamount.setText("AED" + myOrderModelListFrag.get(position).getAmount());


        if(myOrderModelListFrag.get(position).getPayment_type().equals("Online Payment")) {
            holder2.payment_type.setText("Online Payment");
            holder2.ivOrderim.setImageResource(R.drawable.ic_credit_card_black_24dp);
        }
        // Glide.with(contextorder).load(myOrderModelListfrag.get(position).getImage()).into(holder2.ivOrderim);

        holder2.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, OrderItemActivity.class);
            intent.putExtra("orderid", myOrderModelListFrag.get(position).getOrder_id());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return myOrderModelListFrag.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_orderid)
        TextView tvOrderid;
        @BindView(R.id.tv_orderdate)
        TextView tvOrderdate;
        @BindView(R.id.tv_ordertime)
        TextView tvOrdertime;
        @BindView(R.id.tv_priceorder)
        TextView tvPriceorder;
        @BindView(R.id.tv_priceorderamount)
        TextView tvPriceorderamount;
        @BindView(R.id.iv_orderim)
        ImageView ivOrderim;
        @BindView(R.id.payment_type)
        TextView payment_type;




        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
