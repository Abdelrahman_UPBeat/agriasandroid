package com.nav.appiqo.agriyas.WebApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CodPlaceModel {
    /**
     * error : false
     * error_code : 100
     * message : order placed successfully.
     * data : {"order_id":10,"amount":"500"}
     */
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("error_code")
    @Expose
    private int error_code;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private DataBean data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_id : 10
         * amount : 500
         */
        @SerializedName("order_id")
        @Expose
        private int order_id;
        @SerializedName("amount")
        @Expose
        private String amount;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
