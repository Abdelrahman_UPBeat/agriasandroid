package com.nav.appiqo.agriyas.models;

public class CartModel {

    int imgecart;
    int minusimg;
    int addimg;
    String cartitemname;
    String cartitemprice;
    String cartitemNo;
    public CartModel(int imgecart, int minusimg, int addimg, String cartitemname, String cartitemprice, String cartitemNo) {
        this.imgecart = imgecart;
        this.minusimg = minusimg;
        this.addimg = addimg;
        this.cartitemname = cartitemname;
        this.cartitemprice = cartitemprice;
        this.cartitemNo = cartitemNo;
    }

    public int getImgecart() {
        return imgecart;
    }

    public void setImgecart(int imgecart) {
        this.imgecart = imgecart;
    }

    public int getMinusimg() {
        return minusimg;
    }

    public void setMinusimg(int minusimg) {
        this.minusimg = minusimg;
    }

    public int getAddimg() {
        return addimg;
    }

    public void setAddimg(int addimg) {
        this.addimg = addimg;
    }

    public String getCartitemname() {
        return cartitemname;
    }

    public void setCartitemname(String cartitemname) {
        this.cartitemname = cartitemname;
    }

    public String getCartitemprice() {
        return cartitemprice;
    }

    public void setCartitemprice(String cartitemprice) {
        this.cartitemprice = cartitemprice;
    }

    public String getCartitemNo() {
        return cartitemNo;
    }

    public void setCartitemNo(String cartitemNo) {
        this.cartitemNo = cartitemNo;
    }


}
