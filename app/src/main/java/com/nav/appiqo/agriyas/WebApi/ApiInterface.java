package com.nav.appiqo.agriyas.WebApi;


import com.nav.appiqo.agriyas.models.AmmountMain;
import com.nav.appiqo.agriyas.models.AuthResponce;
import com.nav.appiqo.agriyas.models.GetTokenRequest;
import com.nav.appiqo.agriyas.models.OrderItemsModelClass;
import com.nav.appiqo.agriyas.models.orderTry;

import okhttp3.MultipartBody;
import payment.sdk.android.cardpayment.CardPaymentRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("register")
    Call<SignUpResponse> signup(
            @Field("full_name") String full_name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("country") String country,
            @Field("mobile") String mobile,
            @Field("device_id") String device_id,
            @Field("lang") String lang


    );

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("email") String email,
                           @Field("password") String password,
                           @Field("device_type") String device_type,
                           @Field("device_id") String device_id,
                           @Field("device_token") String device_token,
                           @Field("lang") String lang);


    @FormUrlEncoded
    @POST("verifyOtp")
    Call<OtpResponseModel> otp(
            @Field("email") String email,
            @Field("otp") int otp,
            @Field("device_type") String device_type,
            @Field("device_id") String device_id,
            @Field("device_token") String device_token,
            @Field("lang") String lang
    );


    @FormUrlEncoded
    @POST("createOtp")
    Call<CrtOtpBean> createotp(
            @Field("email") String email,
            @Field("lang") String lang

    );

    @FormUrlEncoded
    @POST("forgotPassword")
    Call<ForgetApiModel> forgotPassword(
            @Field("email") String email,
            @Field("lang") String lang

    );

    @FormUrlEncoded
    @POST("getProfile")
    Call<GetProfile> getProfile(
            @Field("user_id") String user_id,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("editProfile")
    Call<EditProfile> editProfile(
            @Header("device_id") String device_id,
            @Header("security_token") String security_token,
            @Field("user_id") String user_id,
            @Field("full_name") String full_name,
            @Field("mobile") String mobile,
            @Field("country") String country,
            @Field("lang") String lang
    );


    @FormUrlEncoded
    @POST("homepage")
    Call<HomePageModel> homepage(
            @Field("user_id") String user_id,
            @Field("search") String search,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("codOrder")
    Call<CodPlaceModel> codOrder(
            @Field("user_id") String user_id,
            @Field("address") String address,
            @Field("flat_no") String flat_no,
            @Field("landmark") String landmark,
            @Field("item") String item,
            @Field("amount") String amount,
            @Field("mobile") String mobile,
            @Field("lang") String lang,
            @Field("payment_type") String Pt
    );

    @FormUrlEncoded
    @POST("addToCart")
    Call<AddToCartModel> addToCart(
            @Field("user_id") String user_id,
            @Field("item_id") String item_id,
            @Field("quantity") String quantity,
            @Field("price") String price,
            @Field("type") String type,
            @Field("cutting_type") String cutting_type,
            @Field("other_specification") String other_specification,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("getCuttingType")
    Call<CuttingTypeModel> getCuttingType(
            @Field("user_id") String user_id,
            @Field("item_id") String item_id,
            @Field("type") String type,
            @Field("lang") String lang
    );


    @FormUrlEncoded
    @POST("getMyCart")
    Call<GetMyCartModel> getMyCart(
            @Field("user_id") String user_id,
            @Field("lang") String lang
    );


    @Multipart
    @POST("uploadUserProfileImage")
    Call<UploadImageModel> uploadUserProfileImage(
            @Part("user_id") String user_id,
            @Part MultipartBody.Part image,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("deleteCart")
    Call<DeleteCartModel> deleteCart(
            @Field("user_id") String user_id,
            @Field("cart_id") String cart_id,
            @Field("lang") String lang
    );


    @FormUrlEncoded
    @POST("updateCart")
    Call<UpdateCartModel> updateCart(
            @Field("user_id") String user_id,
            @Field("item_id") String item_id,
            @Field("type") String type,
            @Field("quantity") String quantity,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("myOrder")
    Call<MyOrdersModel> myOrder(
            @Field("user_id") String user_id,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("orderItems")
    Call<OrderItemsModelClass> orderItems(
            @Field("order_id") String order_id,
            @Field("lang") String lang
    );


    @FormUrlEncoded
    @POST("token")
    Call<AuthResponce> getTockenForPayment(@Field("grant_type") String grant_type);

    @FormUrlEncoded
    @POST("orders")
    Call<orderTry> getOrderForPayment(@Query("action") String action);//,
    //   @Body AmmountMain amount);
    //    @Query("outletId") String outletId);



/*
    @Multipart
    @POST("edit_profile")
    Call<EditProfile> edit_profile(
            @Part("user_gender") RequestBody user_gender,
            @Part("user_firstname") RequestBody user_firstname,
            @Part("user_lastname") RequestBody user_lastname,
            @Part("user_dob") RequestBody user_dob,
            @Part("user_biography") RequestBody user_biography,
            @Part("user_email") RequestBody user_email,
            @Part("user_mobile") RequestBody user_mobile,
            @Part("user_id") RequestBody user_id,
            @Part MultipartBody.Part userfile
    );


    @POST("get_all_company")
    Call<CompanyBeans> get_all_company();

    @POST("get_category")
    Call<CarCategoryBeans> get_category();

    @FormUrlEncoded
    @POST("get_car_model_by_company")
    Call<CarModelBean> get_car_model_by_company(
            @Field("company_id") String company_id

    );

    @FormUrlEncoded
    @POST("add_car")
    Call<SignUpResponse> add_car(
            @Field("company_id") String company_id,
            @Field("model_id") String model_id,
            @Field("category_id") String category_id,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("offer_ride")
    Call<OfferRideBeans> offer_ride(
            @Field("user_id") String user_id,
            @Field("lat1") String lat1,
            @Field("long1") String long1,
            @Field("lat2") String lat2,
            @Field("long2") String long2,
            @Field("date") String date,
            @Field("time") String time,
            @Field("company_id") String company_id,
            @Field("model_id") String model_id,
            @Field("size") String size,
            @Field("pessangers") String pessangers,
            @Field("issmoking") String issmoking,
            @Field("ispets") String ispets,
            @Field("start_address") String start_address,
            @Field("end_address") String end_address
    );

    @FormUrlEncoded
    @POST("get_car")
    Call<GetMyCar> get_car(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("offer_ride_second")
    Call<BookOfferRideFinal> offer_ride_second(@Field("offer_ride_id") String offer_ride_id,
                                               @Field("price") String price);

    @Multipart
    @POST("upload_document")
    Call<SignUpResponse> upload_document(
            @Part("user_car_id") RequestBody user_car_id,
            @Part MultipartBody.Part userfile1,
            @Part MultipartBody.Part userfile2,
            @Part MultipartBody.Part userfile3,
            @Part MultipartBody.Part userfile4
    );

    @FormUrlEncoded
    @POST("search_ride")
    Call<SearchRideBeans> search_ride(@Field("lat1") String latpickup,
                                      @Field("long1") String longpickup,
                                      @Field("lat2") String latdrop,
                                      @Field("lat2") String longdrop,
                                      @Field("date") String date); @FormUrlEncoded
    @POST("create_booking")
    Call<CreateBookingBeans> create_booking(@Field("offer_ride_id") String offer_ride_id,
                                            @Field("user_id") String user_id,
                                            @Field("pessangers") String pessangers);







    @FormUrlEncoded
    @POST("past_ride")
    Call<PastRideBeans> past_ride(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("your_ride")
    Call<PastRideBeans> your_ride(@Field("user_id") String userId);


    @FormUrlEncoded
    @POST("delete_ride")
    Call<SignUpResponse> delete_ride(@Field("offer_ride_id") String offer_ride_id);


  @FormUrlEncoded
    @POST("publish_return")
    Call<PublishReturnBeans> publish_return(@Field("offer_ride_id") String offer_ride_id);



*/


}
