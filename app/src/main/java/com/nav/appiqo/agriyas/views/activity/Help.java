package com.nav.appiqo.agriyas.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Help extends AppCompatActivity {

    @BindView(R.id.iv_backarrow)
    ImageView ivBackarrow;
    @BindView(R.id.toolbarprofile)
    LinearLayout toolbarprofile;
    @BindView(R.id.tvemail)
    CustomTextView tvemail;
    @BindView(R.id.tvContact)
    CustomTextView tvContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_backarrow, R.id.tvemail, R.id.tvContact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_backarrow:
                onBackPressed();
                break;
            case R.id.tvemail:
                break;
            case R.id.tvContact:
                break;
        }
    }
}
