package com.nav.appiqo.agriyas.WebApi;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mohamed Usama on 9/4/2019.
 */
public class ApiClientOnlinePayment {

    private static ApiInterface service;
    private static String lang = "en";


    public static ApiInterface getClient() {

        if (service == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

            OkHttpClient.Builder httpClient;
            //      if (BuildConfig.DEBUG) {
            httpClient = getUnsafeOkHttpClient().newBuilder();
            //  } else {
            httpClient = new OkHttpClient.Builder();
            //    }
            // add header to the request
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Authorization", "Basic: YTc4YTBjZTEtOWEyNS00ZjcwLWExMWMtMjU2YTY3YmRhMmFlOmVkY2I5YTIxLTU5Y2YtNGMyNy04OWFkLTcyNjZlZDNhYTJkYQ==")
                        .addHeader("Content-Type", "application/x-www-form-urlencoded");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            });

            // Add logging into retrofit 2.0 if in debug mode
//            if (BuildConfig.DEBUG) {
//                HttpLoggingInterceptor headerLogging = new HttpLoggingInterceptor();
//                headerLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                httpClient.interceptors().add(headerLogging);
//                httpClient.addNetworkInterceptor(new StethoInterceptor());
//            }

            httpClient.connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES);
            //https://identity.ngenius-payments.com/auth/realms/NetworkInternational/protocol/openid-connect/token
            // old https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/
            String baseUrl = "https://identity.ngenius-payments.com/auth/realms/NetworkInternational/protocol/openid-connect/";
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build()).build();

            service = retrofit.create(ApiInterface.class);
        }
        return service;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
