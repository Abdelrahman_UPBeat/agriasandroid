package com.nav.appiqo.agriyas.views.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.EditProfile;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.PrefixEditText;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.helper.Validation;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Profile extends Fragment {

    ImageView imageViewback;
    private static final String DEFAULT_LOCAL = "Portugal";
    String[] array_spinner = new String[100];

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    @BindView(R.id.profileLayout)
    ScrollView profileLayout;
    @BindView(R.id.iv_backarrow)
    ImageView ivBackarrow;
    @BindView(R.id.toolbarprofile)
    Toolbar toolbarprofile;
    @BindView(R.id.relative_layout)
    LinearLayout relativeLayout;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.tv_country)
    TextView tvCountry;
    @BindView(R.id.et_mobile)
    PrefixEditText etMobile;

    public static CircleImageView profilepicedit;

    Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frament_profile, container, false);
        unbinder = ButterKnife.bind(this, v);
        imageViewback = v.findViewById(R.id.iv_backarrow);
        profilepicedit = v.findViewById(R.id.profilepicedit);

        array_spinner = getResources().getStringArray(R.array.country_arrays);

        apiInterface = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiInterface.class);
        progressView = new ProgressView(getContext());
        prefData = new PrefData(getActivity().getApplicationContext());

        setDataForEditProfile();

        profilepicedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        imageViewback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ControllerActivity) getActivity()).displayCaned(new FragmentMainScreen(), "main");
            }
        });


        etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return v;
    }

    private void showDialog() {
        ImagePicker.with(getActivity())
                .setToolbarColor("#212121")
                .setStatusBarColor("#000000")
                .setToolbarTextColor("#FFFFFF")
                .setToolbarIconColor("#FFFFFF")
                .setProgressBarColor("#4CAF50")
                .setBackgroundColor("#212121")
                .setCameraOnly(false)
                .setMultipleMode(false)
                .setFolderMode(true)
                .setShowCamera(true)
                .setFolderTitle("Albums")
                .setImageTitle("Galleries")
                .setDoneTitle("Done")
                .setKeepScreenOn(true)
                .start();

    }

    private void setDataForEditProfile() {
        Log.e("picc","ouside null "+MyApplication.readStringPref(PrefData.User_userfile));
        if (!MyApplication.readStringPref(PrefData.User_userfile).equalsIgnoreCase(" ")) {
            Log.e("picc","inside null"+MyApplication.readStringPref(PrefData.User_userfile));
            Glide.with(getActivity()).load(MyApplication.readStringPref(PrefData.User_userfile)).apply(new RequestOptions().placeholder(R.drawable.profilepic)).into(profilepicedit);
            // ControllerActivity.file = new File(MyApplication.readStringPref(PrefData.profilepic));
            ControllerActivity.file = new File(MyApplication.readStringPref(PrefData.User_userfile));
        }
        etUsername.setText(MyApplication.readStringPref(PrefData.full_name));
        etEmail.setText(MyApplication.readStringPref(PrefData.user_email));
        etMobile.setText(MyApplication.readStringPref(PrefData.user_mobile));
        tvCountry.setText("UAE");
    }

    @OnClick(R.id.btn_save)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_save: {
                if (Validation.nullValidator(etUsername.getText().toString())) {
                    Utils.showSnackBar(profileLayout, "Enter user name", etUsername, getActivity());
                } else if (Validation.nullValidator(etEmail.getText().toString())) {
                    Utils.showSnackBar(profileLayout, "Enter email id", etEmail, getActivity());
                } else if (!Validation.emailValidator(etEmail.getText().toString())) {
                    Utils.showSnackBar(profileLayout, "Enter valid email id format", etEmail, getActivity());
                } else if (Validation.nullValidator(etMobile.getText().toString())) {
                    Utils.showSnackBar(profileLayout, "Enter phone number", etMobile, getActivity());
                } else if (!Validation.isValidMobile(etMobile.getText().toString())) {
                    Utils.showSnackBar(profileLayout, "Enter phone number", etMobile, getActivity());
                } else {
                    updateinfo();
                }
                break;
            }
        }
    }

    private void updateinfo() {
        progressView.showLoader();
        String full_name = etUsername.getText().toString();
        String mobile = etMobile.getText().toString();

        Call<EditProfile> call = apiInterface.editProfile(
                MyApplication.readStringPref(PrefData.deviceId),
                MyApplication.readStringPref(PrefData.securityToken),
                MyApplication.readStringPref(PrefData.USER_ID),
                full_name,
                mobile,
                "UAE",Util.getLocale());
        call.enqueue(new Callback<EditProfile>() {
            @Override
            public void onResponse(Call<EditProfile> call, Response<EditProfile> response) {
                progressView.hideLoader();

                if (!response.body().isError()) {

                    btnSave.setEnabled(false);
                    MyApplication.writeStringPref(PrefData.full_name, response.body().getData().getFull_name());
                    ControllerActivity.navUsername.setText(response.body().getData().getFull_name());
                    ControllerActivity.mobileNumber.setText(response.body().getData().getMobile());
                    MyApplication.writeStringPref(PrefData.user_mobile, response.body().getData().getMobile());
                    MyApplication.writeStringPref(PrefData.User_userfile, response.body().getData().getImage());
                    MyApplication.writeStringPref(PrefData.user_email, response.body().getData().getEmail());

                    Util.showSnackBar(profileLayout, response.body().getMessage(), getActivity());
                } else {
                    Util.showSnackBar(profileLayout, response.body().getMessage(), getActivity());
                }
            }

            @Override
            public void onFailure(Call<EditProfile> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}