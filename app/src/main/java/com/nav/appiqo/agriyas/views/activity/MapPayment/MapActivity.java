package com.nav.appiqo.agriyas.views.activity.MapPayment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.PayMent;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.TryAct;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.CodPlaceModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.helper.Validation;
import com.nav.appiqo.agriyas.models.Ammount;
import com.nav.appiqo.agriyas.models.AmmountMain;
import com.nav.appiqo.agriyas.views.activity.MapPayment.Presenter.TokenPresenter;
import com.nav.appiqo.agriyas.views.activity.OrderConfirrmation;
import com.nav.appiqo.agriyas.views.fragment.FragmentConfimation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import payment.sdk.android.PaymentClient;
import payment.sdk.android.cardpayment.CardPaymentData;
import payment.sdk.android.cardpayment.CardPaymentRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnCameraChangeListener, MapTockenContract.IpaymentView {


    private static final int PAYMENT_TYPE_COD = 2;
    private static final int PAYMENT_TYPE_CARD = 1;
    private static final int PAYMENT_TYPE_NUN = 0;
    public static int MY_PERMISSION_FINE_LOCATION = 101;
    private static final int PAYMENT_REQUIST = 1;

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.pin)
    ImageView pin;
    List<Address> addresses;
    RadioButton rb;
    @BindView(R.id.et_flatnumber)
    TextInputEditText etFlatnumber;
    @BindView(R.id.et_fulladdress)
    TextInputEditText et_fulladdress;
    @BindView(R.id.phone)
    TextInputEditText phone;
    @BindView(R.id.et_area)
    TextInputEditText etArea;
    @BindView(R.id.et_landmark)
    TextInputEditText etLandmark;
    @BindView(R.id.lnrlayut2)
    LinearLayout lnrlayut2;
    @BindView(R.id.tv_submitMapLocation)
    TextView tvSubmitMapLocation;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;

    private GoogleApiClient mgoogleapiclient;
    private GoogleApiClient placeGoogleApi;
    private LatLng mylatlang;
    private Location myLocation;
    private Intent intent;
    private GoogleMap nmap;
    private Marker marker;
    private String name;
    private LatLng mlatlang;
    // private GeoDataClient mGeoDataClient;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private String item = "", address = "", totalAmount = "";
    private int paymentMode = PAYMENT_TYPE_NUN;
    private ProgressView progressView;
    private ApiInterface apiInterface;
    private int REQUEST_CHECK_SETTINGS = 10;
    private int ACCESS_FINE_LOCATION_INTENT_ID = 101;

    CardPaymentRequest req;

    TokenPresenter tocken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        progressView = new ProgressView(MapActivity.this);
        apiInterface = ApiClient.getClient(MapActivity.this).create(ApiInterface.class);
        Bundle bundle = new Bundle();
        bundle = getIntent().getBundleExtra("bundle");
        etFlatnumber.setEnabled(false);


        addresses = new ArrayList<>();
        // checkPermissions();
        try {
            if (googleserviceavailable()) {
                final MapFragment nmap = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

                nmap.getMapAsync(this);
                mgoogleapiclient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }

        } catch (Exception e) {

        }

        item = bundle.getString("jsaonData");
        totalAmount = bundle.getString("finalAmountOfOrder");

        tocken = new TokenPresenter(this);
    }


    public void GetAdresss() {
        if (mlatlang.longitude > 0.0) {
            Geocoder geocoder;
            geocoder = new Geocoder(this, Locale.getDefault());
            Log.i("camera", "GetAdresss: " + mlatlang.latitude + "  +" + mlatlang.longitude);


            try {
                addresses = geocoder.getFromLocation(mlatlang.latitude, mlatlang.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                // String city = addresses.get(0).getLocality();
//            Log.i("camera", "onCameraChange: " + address);

                //           Log.i("camera", "onCameraChange:++++++++++ " + addresses);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e1) {
                e1.printStackTrace();
                GetAdresss();
            }

            //  Log.i("camera", "onCameraChange: " + address);
            try {
                etFlatnumber.setText(addresses.get(0).getAddressLine(0));
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                GetAdresss();
                GetAdresss();
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            nmap = googleMap;
            nmap.setOnCameraIdleListener(onCameraIdleListener);


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                nmap.setMyLocationEnabled(true);
                nmap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);

                }
            }

            LocationManager mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = mLocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch (Exception e) {
            e.getMessage();
        }


        // map.setOptions({draggable: true});
        try {
            if (!nmap.isMyLocationEnabled())
                nmap.setMyLocationEnabled(true);

        } catch (Exception e) {
        }
        try {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (myLocation == null) {

                //  nmap.clear();

                Location location = LocationServices.FusedLocationApi.getLastLocation(mgoogleapiclient);
                if (location != null) {

                    mlatlang = new LatLng(location.getLatitude(), location.getLongitude());
                    nmap.animateCamera(CameraUpdateFactory.newLatLngZoom(mlatlang, 12));
                    Log.e("onMapReady: latlang ", mlatlang.toString());

                    //  markeroptions(mlatlang);


                }
            } else {
                mlatlang = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                nmap.animateCamera(CameraUpdateFactory.newLatLngZoom(mlatlang, 12));
                Log.e("onMapReady: latlang ", mlatlang.toString());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            nmap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    Log.i("camera", "onCameraChange: " + cameraPosition.target.longitude + "   " + cameraPosition.target.longitude);
                }
            });

            nmap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    mlatlang = nmap.getCameraPosition().target;
                    Log.i("camera", "onCameraIdle: " + mlatlang.longitude + "   " + mlatlang.latitude);
                    GetAdresss();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("camera", "onLocationChanged: " + location.getLatitude());

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mgoogleapiclient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d("tag", "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mgoogleapiclient, (com.google.android.gms.location.LocationListener) this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mgoogleapiclient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void changeMap(Location location) {

        //  Log.d(TAG, "Reaching map" + mMap);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // check if map is created successfully or not
        if (nmap != null) {
            nmap.getUiSettings().setZoomControlsEnabled(true);
            LatLng latLong;


            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(19f).tilt(70).build();

            nmap.setMyLocationEnabled(true);
            nmap.getUiSettings().setMyLocationButtonEnabled(true);
            nmap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            //   mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            //     startIntentService(location);


        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    public void paymentdialog(Context con) {
        // dialog1.dismiss();

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(con);
        final View sheetView = MapActivity.this.getLayoutInflater().inflate(R.layout.paymentdialog, null);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

        final RadioButton rb1, rb3;
        RadioGroup rgPaymentMode;

        TextView Tv_cancel, Tv_OK;
        rb1 = sheetView.findViewById(R.id.rb_payment);
        rb3 = sheetView.findViewById(R.id.rb_payment3);
        rgPaymentMode = sheetView.findViewById(R.id.rg_payment_mode);
        Tv_cancel = sheetView.findViewById(R.id.tv_cancel);
        Tv_OK = sheetView.findViewById(R.id.tv_ok);

        rgPaymentMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton) group.findViewById(checkedId);

                switch (checkedId) {
                    case R.id.rb_payment:
                        paymentMode = PAYMENT_TYPE_CARD;
                        Log.e("hello", "card");
                        break;
                    case R.id.rb_payment3:
                        paymentMode = PAYMENT_TYPE_COD;
                        Log.e("hello", "cod");
                        break;
                }
            }
        });

        Tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        Tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentMode == PAYMENT_TYPE_CARD) {
                    //             Toast.makeText(MapActivity.this, "Currently this feature is unavailable", Toast.LENGTH_SHORT).show();

                    // we will work here

                    //         req = new CardPaymentRequest.Builder();

//                    CardPaymentRequest.Builder b = new CardPaymentRequest.Builder();
//                    String API_KEY ="OWJjMjcwNDEtODI2OS00OTQwLTlhZjctZmUxYjAwZDUyYzVkOjIxNGFiZjBjLTNjMTEtNDJjMS05NzU1LTcwMmM3MGMxOTdiZg==";
//                    b.gatewayUrl("");
//                    b.code("");
//                    req = b.build();
//                    PayMent p = new PayMent(req,MapActivity.this);
                    //  getTockenForPayment

//                    WebView theWebPage = new WebView(MapActivity.this);
//                    theWebPage.getSettings().setJavaScriptEnabled(true);
//                    theWebPage.getSettings().setPluginState(WebSettings.PluginState.ON);
//                    setContentView(theWebPage);
//                    theWebPage.loadUrl("https://paypage-uat.ngenius-payments.com/?code=bf6c26a541d68521");


//


                    Log.d("tokennnnn", "entered");
                    tocken.getTockenForPayment("client_credentials");
                    mBottomSheetDialog.dismiss();


                } else if (paymentMode == PAYMENT_TYPE_NUN) {
                    Toast.makeText(MapActivity.this, "Please Select One of the Payment Method", Toast.LENGTH_SHORT).show();
                } else if (paymentMode == PAYMENT_TYPE_COD) {
                    connectApiToPlaceOrder("COD");
                }
            }
        });


        mBottomSheetDialog.show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        Log.i("camera", "onCameraChange: " + cameraPosition.target.latitude + "   " + cameraPosition.target.longitude);

    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public boolean googleserviceavailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isavailabe = api.isGooglePlayServicesAvailable(this);
        if (isavailabe == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isavailabe)) {

            Dialog dialog = api.getErrorDialog(this, isavailabe, 0);
            dialog.show();

        } else {
            Toast.makeText(MapActivity.this, "can not connect to play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }


    private void connectApiToPlaceOrder(String PayMethod) {
        address = etFlatnumber.getText().toString() + "," + etArea.getText().toString() + "," + etLandmark.getText().toString();

        tocken.postOrder(MyApplication.readStringPref(PrefData.USER_ID),address,etFlatnumber.getText().toString(),etLandmark.getText().toString(),item,totalAmount,phone.getText().toString(),"ar"
        ,MapActivity.this,PayMethod);


        progressView.showLoader();
//        Call<CodPlaceModel> call = apiInterface.codOrder(
//                MyApplication.readStringPref(PrefData.USER_ID),
//                address,
//                etFlatnumber.getText().toString(),
//                etLandmark.getText().toString(),
//                item,
//                totalAmount, phone.getText().toString(), Util.getLocale());
//        call.enqueue(new Callback<CodPlaceModel>() {
//            @Override
//            public void onResponse(Call<CodPlaceModel> call, Response<CodPlaceModel> response) {
//                progressView.hideLoader();
//                if (!response.body().isError()) {
//
//                    //  mBottomSheetDialog.dismiss();
//
//                    String orderId = "" + response.body().getData().getOrder_id();
//                    String purchaseAmount = "" + response.body().getData().getAmount();
//                    MyApplication.writeStringPref(PrefData.pucaseamount, purchaseAmount);
//                    Log.e("amount", purchaseAmount);
//                    // ((ControllerActivity)getActivity()).finish();
//                    Bundle bundle = new Bundle();
//                    Bundle bundlecart = new Bundle();
//                    bundle.putString("orderId", orderId);
//                    bundle.putString("purchaseAmount", purchaseAmount);
//                    // bundlecart.putString("purchaseAmount", purchaseAmount);
//                    //set Fragmentclass Arguments
//                    FragmentConfimation fragobj = new FragmentConfimation();
//                    //  Fragment_Cart fragment_cart=new Fragment_Cart();
//                    fragobj.setArguments(bundle);
//                    // fragment_cart.setArguments(bundlecart);
//                    startActivity(new Intent(MapActivity.this, OrderConfirrmation.class).putExtra("orderId", orderId).putExtra("purchaseAmount", purchaseAmount)
//                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//                    //    ((ControllerActivity) getActivity()).displayCaned(fragobj, "confirm");
//                    //   ((ControllerActivity) getActivity()).displayCaned(fragment_cart,"confirm");
//                } else {
//                    //    mBottomSheetDialog.dismiss();
//                    Toast.makeText(MapActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CodPlaceModel> call, Throwable t) {
//                t.printStackTrace();
//                progressView.hideLoader();
//                //mBottomSheetDialog.dismiss();
//
//                if (NetworkUtil.isOnline(MapActivity.this)) {
//                    Toast.makeText(MapActivity.this, "Something gone wrong", Toast.LENGTH_SHORT).show();
//                } else
//                    Toast.makeText(MapActivity.this, "Bad Network", Toast.LENGTH_SHORT).show();
//                t.printStackTrace();
//            }
//        });
    }

    @OnClick({R.id.iv_back, R.id.tv_submitMapLocation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tv_submitMapLocation:

                if (Validation.nullValidator(etArea.getText().toString())) {

                    Util.Error(etArea, "Please Enter House/Flat no.");
                } else if (Validation.nullValidator(etLandmark.getText().toString())) {
                    Util.Error(etLandmark, "Please enter Landmark");
                } else
                    paymentdialog(MapActivity.this);
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MapActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }


    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }


    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mgoogleapiclient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //   updateGPSStatus("GPS is Enabled in your device");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //     locationpermission = true;
                    //If permission granted show location dialog if APIClient is not null
                   /* if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();*/


                } else {
                    //  updateGPSStatus("Location Permission denied.");
                    Toast.makeText(MapActivity.this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // locationpermission = false;
                }
                return;
            }
        }
    }

    public void onDestroy() {

        super.onDestroy();
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null)
            getFragmentManager().beginTransaction()
                    .remove(mapFragment).commit();
    }

    @Inject
    void cardPayment(CardPaymentRequest req) {
        PaymentClient p = new PaymentClient(MapActivity.this);
        p.launchCardPayment(req, 1);

    }


    @Override
    public void onpaymentFailure(String TokenError) {

        Log.d("tokennnnn", "error");
        Toast.makeText(MapActivity.this, "faddy", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onpaymentSuccess(String Tokensuccess) {
        Log.d("Bearer", "Bearer " + Tokensuccess);
     //   Toast.makeText(MapActivity.this, Tokensuccess, Toast.LENGTH_SHORT).show();


        AmmountMain a = new AmmountMain();
        Ammount am = new Ammount();
        am.setCurrencyCode("AED");
        am.setValue(999);
        a.setAmmount(am);

        CardPaymentRequest.Builder w = new CardPaymentRequest.Builder();

     //   double realTotal = Double.parseDouble(totalAmount)*100;

        tocken.call(Tokensuccess, MapActivity.this, MapActivity.this, totalAmount);


    }

    @Override
    public void onOrderFailure(String TokenError) {
        Toast.makeText(MapActivity.this, TokenError, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onOrderSuccess(String x) {


        Log.d("herew", x);
        String spliter[] = x.split("_");


        //    WebView mWebView = new WebView(MapActivity.this);
//            mWebView.getSettings().setJavaScriptEnabled(true);
//            mWebView.loadUrl(x);

//            String url = x;
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);

//        WebView myWebView = new WebView(MapActivity.this);
//        setContentView(myWebView);
//
//        WebSettings webSettings = myWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        myWebView.loadUrl(spliter[0]);

//        String spilt3[] = x.split("=");
//////


        CardPaymentRequest.Builder b = new CardPaymentRequest.Builder();
        CardPaymentRequest req;
////
        Log.d("Auth", spliter[1]);
        b.gatewayUrl(spliter[1]);
        String codeSpliter[] = spliter[0].split("=");
        Log.d("AuthCode", codeSpliter[1]);
        b.code(codeSpliter[1]);
        req = b.build();
//        Log.d("resepswwwww2code", spilt3[1]);
//        req = b.build();

        Intent intent= new Intent(MapActivity.this, TryAct.class);
        intent.putExtra("Auth",spliter[1]);
        intent.putExtra("AuthCode", codeSpliter[1]);
        intent.putExtra("etFlatnumber",etFlatnumber.getText().toString());
        intent.putExtra("etArea",etArea.getText().toString());
        intent.putExtra("etLandmark",etLandmark.getText().toString());
        intent.putExtra("item",item);
        intent.putExtra("totalAmount",totalAmount);
        intent.putExtra("phone",phone.getText().toString());
        startActivity(intent);

     //   PaymentClient p = new PaymentClient(MapActivity.this);
      //  p.launchCardPayment(req, PAYMENT_REQUIST);


    }

    @Override
    public void onOrderPostSuccess(CodPlaceModel codPlaceModel) {
                        progressView.hideLoader();

        String orderId = "" + codPlaceModel.getData().getOrder_id();
        String purchaseAmount = "" + codPlaceModel.getData().getAmount();
        MyApplication.writeStringPref(PrefData.pucaseamount, purchaseAmount);
        Log.e("amount", purchaseAmount);
        // ((ControllerActivity)getActivity()).finish();
        Bundle bundle = new Bundle();
        Bundle bundlecart = new Bundle();
        bundle.putString("orderId", orderId);
        bundle.putString("purchaseAmount", purchaseAmount);
        // bundlecart.putString("purchaseAmount", purchaseAmount);
        //set Fragmentclass Arguments
        FragmentConfimation fragobj = new FragmentConfimation();
        //  Fragment_Cart fragment_cart=new Fragment_Cart();
        fragobj.setArguments(bundle);
        // fragment_cart.setArguments(bundlecart);
        startActivity(new Intent(MapActivity.this, OrderConfirrmation.class).putExtra("orderId", orderId).putExtra("purchaseAmount", purchaseAmount)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onOrderPostFailure(String x) {
                progressView.hideLoader();

        Toast.makeText(getApplicationContext(),x,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

      //  super.onActivityResult(requestCode,resultCode,data);
        Log.d("resultCode",Integer.toString(resultCode));
        Log.d("requestCode",Integer.toString(requestCode));



        if (requestCode == PAYMENT_REQUIST) {

            if (Activity.RESULT_OK == resultCode) {
                CardPaymentData.getFromIntent(data);
                Toast.makeText(getApplicationContext(), "payment done", Toast.LENGTH_LONG).show();
                Log.d("doneww","Paid");
                connectApiToPlaceOrder("Online Payment");

            }
            else {

                Toast.makeText(getApplicationContext(), "payment Didn't paid", Toast.LENGTH_LONG).show();
                Log.d("doneww","not Paid");
           //     connectApiToPlaceOrder("Online Payment");



            }

        }else {

            Log.d("doneww", "meshda5el");
        }

    }


}




