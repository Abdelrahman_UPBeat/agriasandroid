package com.nav.appiqo.agriyas.WebApi;

import java.util.List;

public class CuttingTypeModel {

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBeanCutting> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBeanCutting> getData() {
        return data;
    }

    public void setData(List<DataBeanCutting> data) {
        this.data = data;
    }

    public static class DataBeanCutting {
        /**
         * item_id : 9
         * item_name : BBQ box
         * image : http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698814213b87d49-0912-4119-87e2-881b49d4e4b3.jpg
         * cart_id : 51
         * price : 300
         * quantity : 1
         */

        private String id;
        private String type;
        private String status;
        private String type_ar;

        public String getType_ar() {
            return type_ar;
        }

        public void setType_ar(String type_ar) {
            this.type_ar = type_ar;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
