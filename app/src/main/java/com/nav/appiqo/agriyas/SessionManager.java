package com.nav.appiqo.agriyas;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by obadmin on 8/3/18.
 */

public class SessionManager {





    public static void setFcmKey(Context context, String fcm_key) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        android.content.SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("FcmKey", fcm_key);
        prefsEditor.commit();
    }

    public static String getFcmKey(Context context) {
        android.content.SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("FcmKey", "");
    }







}
