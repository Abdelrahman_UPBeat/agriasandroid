package com.nav.appiqo.agriyas.models;

public class Fav_modelclass {
    public Fav_modelclass(int image, String favitems, String favdesp) {
        this.image = image;
        this.favitems = favitems;
        this.favdesp = favdesp;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getFavitems() {
        return favitems;
    }

    public void setFavitems(String favitems) {
        this.favitems = favitems;
    }

    public String getFavdesp() {
        return favdesp;
    }

    public void setFavdesp(String favdesp) {
        this.favdesp = favdesp;
    }

    int image;
    String favitems;
    String favdesp;
}
