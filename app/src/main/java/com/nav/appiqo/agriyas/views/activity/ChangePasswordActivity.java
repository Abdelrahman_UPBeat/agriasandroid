package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.ForgetApiModel;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.helper.Validation;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.tv_frtpass1)
    TextView tvFrtpass1;
    @BindView(R.id.changePasswordEnterPassword)
    TextInputEditText emailEditTextfpass;
    @BindView(R.id.changePasswordEnterPasswordTIL)
    TextInputLayout emailTextInputLayoutfpass;


    @BindView(R.id.changePasswordReEnterPassword)
    TextInputEditText changePasswordReEnterPassword;
    @BindView(R.id.changePasswordReEnterPasswordTIL)
    TextInputLayout changePasswordReEnterPasswordTIL;



    @BindView(R.id.signInButtonfpass)
    Button signInButtonfpass;


    Locale myLocale;
    @BindView(R.id.foret_layout)
    LinearLayout foretLayout;

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        prefData = new PrefData(ChangePasswordActivity.this);
        progressView = new ProgressView(ChangePasswordActivity.this);
        apiInterface = ApiClient.getClient(ChangePasswordActivity.this).create(ApiInterface.class);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString("Locale_KeyValue", "");
        changeLocale(name);

        signInButtonfpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Validation.nullValidator(emailEditTextfpass.getText().toString())){
                    Utils.showSnackBar(foretLayout, getResources().getString(R.string.PleaseEnterEmailID),
                            emailEditTextfpass, ChangePasswordActivity.this);
                }
                else if (!Validation.emailValidator(emailEditTextfpass.getText().toString())) {
                    Utils.showSnackBar(foretLayout,  getResources().getString(R.string.PleaseEnterValidEmailID), emailEditTextfpass, ChangePasswordActivity.this);

                }else  getDataForgetpassword();

            }});



    }


    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase("")) {
            Log.e("lan", "changeLocale: not");
            return;
        }

        myLocale = new Locale(lang);
        Log.e("lan", "changeLocale1: ");//Set Selected Locale

        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//Update the config
        updateViews();//Update texts according to locale
    }

    public void updateViews() {

        tvFrtpass1.setText(R.string.forget_password);
        emailEditTextfpass.setText(R.string.forget_password);
        signInButtonfpass.setText(R.string.sign_in);


    }

    private void getDataForgetpassword() {

            progressView.showLoader();

            Call<ForgetApiModel> call = apiInterface.forgotPassword(emailEditTextfpass.getText().toString(), Util.getLocale() );
            call.enqueue(new Callback<ForgetApiModel>() {
                @Override
                public void onResponse(Call<ForgetApiModel> call, Response<ForgetApiModel> response) {
                    progressView.hideLoader();

                    if (response.body().getError_code() == 100) {
                        Toast.makeText(ChangePasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(ChangePasswordActivity.this,OtpActivity.class));

                        //response.body().setData(response.body().getData());


                    } else {
                        //Utils.showSnackBar(rootOtp, response.body().getMessage(), OtpActivity.this);
                        Toast.makeText(ChangePasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ForgetApiModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }


    }
