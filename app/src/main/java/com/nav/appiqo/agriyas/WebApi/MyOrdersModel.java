package com.nav.appiqo.agriyas.WebApi;

import java.util.List;

public class MyOrdersModel {

    /**
     * error : false
     * error_code : 100
     * message : my order list.
     * data : [{"order_id":"19","amount":"500","booking_dt":"2018-05-28 11:14:10","payment_type":"COD","id":"19","user_id":"47","address":"jaipur","flat_no":"666","landmark":"kkkkk"},{"order_id":"18","amount":"900","booking_dt":"2018-05-28 11:05:26","payment_type":"COD","id":"18","user_id":"47","address":"h,v,b","flat_no":"v","landmark":"b"},{"order_id":"17","amount":"300","booking_dt":"2018-05-28 10:56:49","payment_type":"COD","id":"17","user_id":"47","address":"Haha,Jaja,Hahaj","flat_no":"Haha","landmark":"Hahaj"},{"order_id":"16","amount":"300","booking_dt":"2018-05-28 10:55:35","payment_type":"COD","id":"16","user_id":"47","address":"11,sndjddjd,djdjdjd","flat_no":"sndjddjd","landmark":"djdjdjd"},{"order_id":"15","amount":"300","booking_dt":"2018-05-28 10:49:49","payment_type":"COD","id":"15","user_id":"47","address":"Jajaj,Jsjjs,Jajsk","flat_no":"Jajaj","landmark":"Jajsk"}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_id : 19
         * amount : 500
         * booking_dt : 2018-05-28 11:14:10
         * payment_type : COD
         * id : 19
         * user_id : 47
         * address : jaipur
         * flat_no : 666
         * landmark : kkkkk
         */

        private String order_id;
        private String amount;
        private String booking_dt;
        private String payment_type;
        private String id;
        private String user_id;
        private String address;
        private String flat_no;
        private String landmark;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBooking_dt() {
            return booking_dt;
        }

        public void setBooking_dt(String booking_dt) {
            this.booking_dt = booking_dt;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }
    }
}
