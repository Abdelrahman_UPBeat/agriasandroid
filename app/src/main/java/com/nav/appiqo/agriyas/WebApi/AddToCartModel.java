package com.nav.appiqo.agriyas.WebApi;

import java.util.Collection;
import java.util.List;

public class AddToCartModel {
    /**
     * error : false
     * error_code : 100
     * message : item added in cart.
     * data : [{"cart_id":"1","item_name":"Item1","image":"http://www.berkeleywellness.com/sites/default/files/field/image/iStock_000000352423Medium_988_380.jpg","price":"5000","quantity":"2"}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cart_id : 1
         * item_name : Item1
         * image : http://www.berkeleywellness.com/sites/default/files/field/image/iStock_000000352423Medium_988_380.jpg
         * price : 5000
         * quantity : 2
         */
        private String item_id;
        private String cart_id;
        private String item_name;
        private String image;
        private String price;
        private String quantity;


        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getCart_id() {
            return cart_id;
        }

        public void setCart_id(String cart_id) {
            this.cart_id = cart_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
