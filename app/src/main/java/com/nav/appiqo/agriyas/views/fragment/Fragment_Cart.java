package com.nav.appiqo.agriyas.views.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.CustomTextView;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.GetMyCartModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.adapter.CartAdapter;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.SwipeButton;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nav.appiqo.agriyas.views.activity.MapPayment.MapActivity;
import com.nav.appiqo.agriyas.views.activity.SignInActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Cart extends Fragment {
    public static String CallingFragment = "";
    @BindView(R.id.swipe_btn)
    public  SwipeButton swipeBtn;
    public Boolean flat = true;
    public int finalAmountOfOrder = 0;
    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    @BindView(R.id.iv_menutoolcart)
    ImageView ivMenutoolcart;
    @BindView(R.id.tv_toolbartiltlecart)
    TextView tvToolbartiltlecart;
    @BindView(R.id.toolbarcart)
    Toolbar toolbarcart;
    @BindView(R.id.cartmain)
    LinearLayout cartmain;
    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;
    CartAdapter cartAdapter;
    List<GetMyCartModel.DataBean> myOrderModelListcart;
    int totalAmount;
    JSONObject paramObject;
    JSONArray jsonArray;
    @BindView(R.id.tvContinue)
    CustomTextView tvContinue;
    int refreshamount = 0;
    int ACCESS_FINE_LOCATION_INTENT_ID = 101;
    Unbinder unbinder;
    boolean locationpermission = false;
    private int i;
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this, v);
        sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        myOrderModelListcart = new ArrayList<>();


        Bundle bundle = this.getArguments();
        if (this.getArguments() != null) {
            CallingFragment = bundle.getString("cart");
        }

        swipedisable();


        progressView = new ProgressView(getActivity());
        apiInterface = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiInterface.class);
        prefData = new PrefData(getActivity().getApplicationContext());

        ivMenutoolcart.setOnClickListener(view -> ((ControllerActivity) getActivity()).drawer());


        tvContinue.setOnClickListener(view -> ((ControllerActivity) getActivity()).displayCaned(new FragmentMainScreen(), "main"));
      /*  floatingbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ControllerActivity) getActivity()).displayCaned(new Fragment_Mainscreen(), "main");
            }
        });*/
//swipeBtn.setText(MyApplication.readStringPref(PrefData.pucaseamount)+"45");
       /* rvCart.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvCart, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                ImageView ivCartminus=view.findViewById(R.id.iv_cartminus);
                ImageView ivCartdd= view.findViewById(R.id.iv_cartdd);
                final TextView tvItemnocart=view.findViewById(R.id.tv_itemnocart);

                final TextView tvTotalamount=view.findViewById(R.id.tv_totalamount);

                ivCartdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        i = Integer.parseInt(tvItemnocart.getText().toString());
                        i++;
                        String s = Integer.toString(i);
                        refreshamount= i;
                        tvItemnocart.setText(String.valueOf(refreshamount));

                        connectApiAddToCart(myOrderModelListcart.get(position).getItem_id(),myOrderModelListcart.get(position).getPrice());
                        // MyApplication.writeStringPref(PrefData.quantity_in_cart,(String.valueOf(refreshamount)));

                        Integer amount = Integer.parseInt(tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(position).getPrice()));
                        tvTotalamount.setText("AED "+amount);
                    }
                });

                ivCartminus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // connectApiGetMyCart();

                        i = Integer.parseInt(tvItemnocart.getText().toString());
                        if (i > 0) {
                            i--;
                            String t = Integer.toString(i);
                            refreshamount=i;
                           tvItemnocart.setText(String.valueOf(refreshamount));
                            //    MyApplication.writeStringPref(PrefData.quantity_in_cart,(String.valueOf(refreshamount)));
                            Integer amount = Integer.parseInt(tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(position).getPrice()));
                            tvTotalamount.setText("AED "+amount);


                            if (i == 0) {
                               // connectApiDeleteMyCart(cartId, position);
                                connectApiDeleteMyCart(myOrderModelListcart.get(position).getCart_id(),position);

                            }
                        }
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/
        swipeBtn.setOnStateChangeListener(active -> {


            if(ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")){
                Toast.makeText(getActivity(), getString(R.string.login_to_order), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("type", "1");
                startActivity(intent);
            }else{
                //passData();
                checkPermissions();
            }

            //  requestLocationPermission();

            //  requestLocationPermission();
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCart.setLayoutManager(linearLayoutManager);
        cartAdapter = new CartAdapter(getActivity(), myOrderModelListcart, Fragment_Cart.this);
        rvCart.setAdapter(cartAdapter);

        connectApiGetMyCart();


        return v;
    }
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void checkPermissions() {

        if (!isLocationEnabled()) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            getActivity().startActivity(intent);

        }else{
            passData();

            // Write you code here if permission already given.
        }
/*        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                passData();

                //  screentimer();
                Log.i("tag46", "checkpermission");
                //  showSettingDialog();
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        } else {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            getActivity().startActivity(intent);        }*/
    }


    private void passData() {
        if (myOrderModelListcart.size() == 0) {
            Util.showSnackBar(cartmain, getResources().getString(R.string.additemtocart), getActivity());
        } else {

            finalAmountOfOrder = 0;
            jsonArray = new JSONArray();
            try {
                int total = 0;
                for (int i = 0; i < myOrderModelListcart.size(); i++) {
                    paramObject = new JSONObject();
                    paramObject.put("item_id", myOrderModelListcart.get(i).getItem_id());
                    paramObject.put("quantity", myOrderModelListcart.get(i).getQuantity());
                    paramObject.put("price", myOrderModelListcart.get(i).getPrice());
                    totalAmount = Integer.parseInt(myOrderModelListcart.get(i).getQuantity()) * Integer.parseInt(myOrderModelListcart.get(i).getPrice());
                    total = totalAmount + total;
                    paramObject.put("total", String.valueOf(totalAmount));
                    finalAmountOfOrder = finalAmountOfOrder + totalAmount;
                    jsonArray.put(paramObject);


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("jsaonData", jsonArray.toString());

            // MapFragment mapFragment = new MapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("jsaonData", jsonArray.toString());
            bundle.putString("finalAmountOfOrder", "" + finalAmountOfOrder);

            //requestLocationPermission();


            startActivity(new Intent(getActivity(), MapActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("bundle", bundle));


            //startActivity(new Intent(getActivity(), MapActivity.class).putExtra("bundle", bundle));

            //  Toast.makeText(getContext(), "Please grant the location permission", Toast.LENGTH_SHORT).show();


            //
            //mapFragment.setArguments(bundle);

            // ((ControllerActivity) getActivity()).displayCaned(mapFragment, "map");
        }
    }

    /* private void connectApiDeleteMyCart(String cartid, final int pos) {
         progressView.showLoader();

         Call<DeleteCartModel> call = apiInterface.deleteCart(
                 MyApplication.readStringPref(PrefData.user_id),
                 cartid
         );
         call.enqueue(new Callback<DeleteCartModel>() {
             @Override
             public void onResponse(Call<DeleteCartModel> call, Response<DeleteCartModel> response) {
                 progressView.hideLoader();
                 if (!response.body().isError()) {
                     Log.e("yipeee", "yipeeee");

                   //  myOrderModelListcart.remove(pos);
                    // cartAdapter.notifyItemRemoved(pos);
                     connectApiGetMyCart();
                    // notifyItemRemoved(pos);
                     String sizeofcart = String.valueOf(myOrderModelListcart.size());
                     MyApplication.writeStringPref(PrefData.sizeofcart,sizeofcart);

                     Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                 } else {
                     Log.e("yipeee", "oppppssssss");
                     Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onFailure(Call<DeleteCartModel> call, Throwable t) {
                 t.printStackTrace();
                 Log.e("yipeee", "shitttttt");
                 progressView.hideLoader();
             }
         });
     }*/
   /* private void connectApiAddToCart(String itemid,String itemprice) {
        progressView.showLoader();

        String userid = MyApplication.readStringPref(PrefData.user_id);
        String quantity ="1";
        String type = "add";
        String CuttingType = MyApplication.readStringPref(PrefData.cutting_type);
        MyApplication.writeStringPref(PrefData.cutting_type,CuttingType);
        String Oterspecification = "";


        Call<AddToCartModel> call = apiInterface.addToCart(userid, itemid, quantity, itemprice, type, CuttingType, Oterspecification);
        call.enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                progressView.hideLoader();

                if (!response.body().isError()) {


                    Toast.makeText(getContext(), "Added succesfully", Toast.LENGTH_SHORT).show();
                    connectApiGetMyCart();
                    //  dataBeans.clear();
                    //  dataBeans.addAll(response.body().getData());

                    //   dialog1.dismiss();

                    Fragment_Cart fragCart = new Fragment_Cart();
                    Bundle bundle = new Bundle();
                    //   bundle.putString("cart", "fromItemDescription");
                    //   fragCart.setArguments(bundle);

                    //  ((ControllerActivity) getActivity()).displayCaned(fragCart, "fragCart");
                } else {
                    //  Util.showSnackBar(mainlayoutdescp, response.body().getMessage(), getActivity());
                    //   dialog1.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                t.printStackTrace();
                progressView.hideLoader();
            }
        });
    }
*/
    @Override
    public void onResume() {
        super.onResume();
        flat = false;
    }


    public void swipedisable() {
        if (myOrderModelListcart.size() != 0) {
            swipeBtn.setEnabled(true);
        } else {
            swipeBtn.setEnabled(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void connectApiGetMyCart() {
        progressView.showLoader();

        String userId;
        if (ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")) {
            userId = MyApplication.readStringPref(PrefData.DEVICE_ID);
        } else {
            userId = MyApplication.readStringPref(PrefData.USER_ID);
        }

        Log.i("user", "connectApiGetMyCart: " + userId);
        Call<GetMyCartModel> call = apiInterface.getMyCart(userId, sharedPreferences.getString(Locale_KeyValue, ""));
        call.enqueue(new Callback<GetMyCartModel>() {
            @Override
            public void onResponse(Call<GetMyCartModel> call, Response<GetMyCartModel> response) {
                progressView.hideLoader();

                try{
                    if (!response.body().isError()) {
                        myOrderModelListcart.clear();
                        finalAmountOfOrder = 0;
                        myOrderModelListcart.addAll(response.body().getData());

                        String sizeofcart = String.valueOf(myOrderModelListcart.size());
                        cartAdapter.notifyDataSetChanged();
                        MyApplication.writeStringPref(PrefData.sizeofcart, sizeofcart);
                        swipedisable();

                        for (GetMyCartModel.DataBean dataBean : myOrderModelListcart) {
                            int total = Integer.parseInt(dataBean.getPrice()) * Integer.parseInt(dataBean.getQuantity());
                            finalAmountOfOrder = finalAmountOfOrder + total;

                        }
                        Log.i("amount", "sefbskjdv+" + finalAmountOfOrder);
                        Log.i("amount", "sefbskjdv+" + finalAmountOfOrder);


                        if (myOrderModelListcart.size() == 0) {
                            swipeBtn.setText("Add to Cart");
                            swipeBtn.setVisibility(View.GONE);
                        } else {
                            swipeBtn.setVisibility(View.VISIBLE);
                            swipeBtn.setText(getString(R.string.slide_to_pay) + String.valueOf(finalAmountOfOrder));

                        }

                        if (myOrderModelListcart.size() == 0) {
                            Toast.makeText(getActivity(), R.string.cart_is_empty, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Util.showSnackBar(cartmain, response.body().getMessage(), getActivity());

                    }
                }catch (Exception e){}

            }

            @Override
            public void onFailure(Call<GetMyCartModel> call, Throwable t) {
                if (NetworkUtil.isOnline(getActivity())) {
                    Toast.makeText(getActivity(), "Something gone wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), "Bad Network", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


}
