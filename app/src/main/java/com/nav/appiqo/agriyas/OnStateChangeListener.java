package com.nav.appiqo.agriyas;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}
