package com.nav.appiqo.agriyas.models;

import java.util.List;

public class OrderItemsModelClass {


    /**
     * error : false
     * error_code : 100
     * message : my items list.
     * data : [{"item_name":"Lamb Chops","image":"http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269879050d62fa44-c6c8-442b-a258-35149d684349.jpg","total":"300.00","quantity":"1"},{"item_name":"Hilton Portugal","image":"http://www.bsmadvisers.com/api/jazzar/api/uploads/item/1526992223Hilton-Portugal-Partnership-to-supply-meat-nationwide.jpg","total":"400.00","quantity":"1"}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * item_name : Lamb Chops
         * image : http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269879050d62fa44-c6c8-442b-a258-35149d684349.jpg
         * total : 300.00
         * quantity : 1
         */

        private String item_name;
        private String image;
        private String total;
        private String quantity;

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
