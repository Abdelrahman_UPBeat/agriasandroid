package com.nav.appiqo.agriyas.WebApi;

import java.util.List;

public class ApiResponse {


    /**
     * iscaradded : true
     * status : true
     * message : Login Successfully
     * result : [{"user_id":"15","full_name":"utkarsh srivastava","user_lastname":"","user_dob":"06/8/1994","user_gender":"Male","user_email":"utkarsh@appiqo.com","user_mobile":"9889778886","user_password":"qwerty","user_userfile":"","user_biography":"","user_created":"","device_token":"pref_deviceid","os_type":"Android"}]
     */

    private boolean status;
    private String message;
    private List<ResultBean> result;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * user_id : 15
         * full_name : utkarsh srivastava
         * user_lastname :
         * user_dob : 06/8/1994
         * user_gender : Male
         * user_email : utkarsh@appiqo.com
         * user_mobile : 9889778886
         * user_password : qwerty
         * user_userfile :
         * user_biography :
         * user_created :
         * device_token : pref_deviceid
         * os_type : Android
         */

        private String full_name;
        private String email;
        private String password;
        private String country;
        private String mobile;


    }
}
