package com.nav.appiqo.agriyas.models;

public class MainScreenModel {
    public MainScreenModel(int image, String bodyparts, String price) {
        this.image = image;
        Bodyparts = bodyparts;
        this.price = price;
    }

    public int image;
    public String Bodyparts;
    public String price;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getBodyparts() {
        return Bodyparts;
    }

    public void setBodyparts(String bodyparts) {
        Bodyparts = bodyparts;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



}
