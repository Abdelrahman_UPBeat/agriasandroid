package com.nav.appiqo.agriyas.views;

public interface OnActiveListener {
    void onActive();
}
