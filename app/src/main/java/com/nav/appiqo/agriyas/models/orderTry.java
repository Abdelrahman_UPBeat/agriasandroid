package com.nav.appiqo.agriyas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class orderTry {


    public Link get_links() {
        return _links;
    }

    public void set_links(Link _links) {
        this._links = _links;
    }

    @SerializedName("_links")
    @Expose
    private Link _links;
}
