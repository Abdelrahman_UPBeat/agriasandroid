package com.nav.appiqo.agriyas.WebApi;

import java.util.List;

public class OrderCartmodel {

    /**
     * error : false
     * error_code : 100
     * message : my order list.
     * data : [{"item_name":"Item2","image":"http://localhost/api/mfa/api/uploads/item/slider1.jpg","total":"60.00","quantity":"4"},{"item_name":"Item1","image":"","total":"60.00","quantity":"2"},{"item_name":"Item2","image":"http://localhost/api/mfa/api/uploads/item/slider1.jpg","total":"60.00","quantity":"4"}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * item_name : Item2
         * image : http://localhost/api/mfa/api/uploads/item/slider1.jpg
         * total : 60.00
         * quantity : 4
         */

        private String item_name;
        private String image;
        private String total;
        private String quantity;

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
