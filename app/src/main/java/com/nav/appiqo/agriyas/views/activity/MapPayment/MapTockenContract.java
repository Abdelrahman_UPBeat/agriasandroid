package com.nav.appiqo.agriyas.views.activity.MapPayment;

import android.content.Context;

import com.nav.appiqo.agriyas.WebApi.CodPlaceModel;
import com.nav.appiqo.agriyas.models.AmmountMain;

import payment.sdk.android.cardpayment.CardPaymentRequest;
import retrofit2.http.Field;

public interface MapTockenContract {

    interface IpaymentPresenter {

        void getTockenForPayment(String client_credentials);
        void getOrderForPayment(String action, AmmountMain amount,String outletId,String Token);
        void postOrder(String user_id,
                       String address,
                       String flat_no,
                       String landmark,
                       String item,
                       String amount,
                       String mobile,
                       String lang,
                       Context context,
                       String payMethod);

    }

    interface IpaymentView {
        void onpaymentFailure(String TokenError);

        void onpaymentSuccess(String Tokensuccess);


        void onOrderFailure(String TokenError);

        void onOrderSuccess(String x);

        void onOrderPostSuccess(CodPlaceModel codPlaceModel);

        void onOrderPostFailure(String x);
    }


}
