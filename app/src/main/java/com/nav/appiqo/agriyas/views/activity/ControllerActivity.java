package com.nav.appiqo.agriyas.views.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.GetProfile;
import com.nav.appiqo.agriyas.WebApi.UploadImageModel;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.fragment.FragmentConfimation;
import com.nav.appiqo.agriyas.views.fragment.FragmentMainScreen;
import com.nav.appiqo.agriyas.views.fragment.Fragment_Cart;
import com.nav.appiqo.agriyas.views.fragment.Fragment_Orders;
import com.nav.appiqo.agriyas.views.fragment.Fragment_Profile;
import com.nav.appiqo.agriyas.views.fragment.GettingStartFragment;
import com.nav.appiqo.agriyas.views.fragment.ItemDescriptionFragment;
import com.nav.appiqo.agriyas.views.fragment.MapFragment;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpPost;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.body.MultipartFormDataBody;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ControllerActivity extends AppCompatActivity {

    private static final String TAG = ControllerActivity.class.getSimpleName();
    public static String guestLogin = "";


    public static String fromOrderConfirmation = "0";
    public static TextView navUsername;
    public static TextView mobileNumber;
    public static File file = null;
    private static SharedPreferences sharedPreferences;
    private static Locale myLocale;
    private static SharedPreferences.Editor editor;
    public Context ctxt;
    public Menu menu;
    public MenuItem itemCart;
    public LinearLayout home;
    public LinearLayout Orders;
    public LinearLayout language;
    public LinearLayout account;
    public LinearLayout cart;
    public TextView carttxtcartcounter;
    public LinearLayout Help;
    public LinearLayout Logout;
    public LinearLayout Sinin;
    public LinearLayout Sinup;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.iv_menutool)
    ImageView ivMenutool;
    @BindView(R.id.tv_toolbartiltle)
    TextView tvToolbartiltle;
    @BindView(R.id.cl_layout)
    LinearLayout clLayout;
    Toolbar toolbar;
    NavigationView navigationView;
    View headerView;
    ProgressView progressView;
    ApiInterface apiInterface;
    RecyclerView recyclerView;
    ImageView imageViewprofile;
    int cartCounter;
    boolean key = false;
    private String type = "";
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        ButterKnife.bind(this);


        sharedPreferences = getSharedPreferences(GettingStartFragment.Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        progressView = new ProgressView(ControllerActivity.this);
        apiInterface = ApiClient.getClient(ControllerActivity.this).create(ApiInterface.class);


        drawerLayout.setEnabled(true);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        setupToolbar();
        hideKeyboard();

        try{
            String lang = sharedPreferences.getString(Locale_KeyValue, "");
            changeLocale(lang);

        }catch (Exception e){}
        //  key="empty";

        //  if (key.equalsIgnoreCase(""))
        initialize();
//setDefaultValues();
        //  initializeCountDrawer();
       /* cartCounter = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_cart));
*/
        // initializeCountDrawer();
        displayCaned(new FragmentMainScreen(), "main");
       /* if (fromOrderConfirmation.equalsIgnoreCase("confirm")){
            displayCaned(new Fragment_Orders(), "order");

        }else {


            displayCaned(new Fragment_Mainscreen(), "main");
        }*/

      /*  Boolean cart = getIntent().getBooleanExtra("fromConfirmation",false);
        Log.e("confmmmmm","outside"+"      "+cart);
        if (cart) {
            Log.e("confmmmmm","inside"+"      "+cart);
                displayCaned(new Fragment_Orders(), "order");
            }
*/

        try {
            type = getIntent().getStringExtra("type");
            if (type.equals("1")) {
                Fragment_Cart fragCart = new Fragment_Cart();
                Bundle bundle = new Bundle();
                bundle.putString("cart", "fromDrawer");
                fragCart.setArguments(bundle);
                displayCaned(fragCart, "cart");
                drawerLayout.closeDrawers();
            }

        } catch (Exception e) {
            type = "";
        }
        if (guestLogin != null) {
            if (guestLogin.equalsIgnoreCase("guestLogin")) {
                hideMenuItems();
            }
        } else {

            showMenuItems();
            //  setDefaultValues();
        }

        Log.i("tag46", "initialize:s " + key);
        try {
            if (!key) {
                setDefaultValues();
            } else {
                if (key) {
                    chanzfrag(new Fragment_Orders(), "order");
                    setDefaultValues();
                } else {
                    setDefaultValues();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                clLayout.setTranslationX(slideX);
            }
        };
        connectGetprofile();


        drawerLayout.addDrawerListener(actionBarDrawerToggle);


       /* navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.nav_Resturants) {
                    displayCaned(new Fragment_Mainscreen(), "main");
                    drawerLayout.closeDrawers();
                } else if (id == R.id.nav_PastOrder) {
                    displayCaned(new Fragment_Orders(), "order");
                    drawerLayout.closeDrawers();
                } else if (id == R.id.nav_setting) {
                    displayCaned(new Fragment_Profile(), "profile");
                    drawerLayout.closeDrawers();
                } else if (id == R.id.nav_cart) {
                    Fragment_Cart fragCart = new Fragment_Cart();
                    Bundle bundle = new Bundle();
                    bundle.putString("cart", "fromDrawer");
                    fragCart.setArguments(bundle);
                    displayCaned(fragCart, "cart");
                    drawerLayout.closeDrawers();
                } else if (id == R.id.Help) {
                    drawerLayout.closeDrawers();
                } else if (id == R.id.logout) {
                    logOut();
                } else if (id == R.id.signin) {
                    startActivity(new Intent(ControllerActivity.this, SignInActivity.class));
                    finish();
                    drawerLayout.closeDrawers();
                } else if (id == R.id.signup) {
                    startActivity(new Intent(ControllerActivity.this, SignUpActivity.class));
                    finish();
                    drawerLayout.closeDrawers();
                }
                return true;
            }
        });*/
    }

    public void disableDrwable() {
        drawerLayout.setEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    //Get locale method in preferences
    public String loadLocaleShaired() {
        return sharedPreferences.getString(GettingStartFragment.Locale_KeyValue, "en");

    }

    private void showMenuItems() {
       /* menu = navView.getMenu();
        menu.findItem(R.id.nav_Resturants).setVisible(true);
        menu.findItem(R.id.nav_PastOrder).setVisible(true);
        menu.findItem(R.id.nav_setting).setVisible(true);
        menu.findItem(R.id.nav_cart).setVisible(true);
        menu.findItem(R.id.Help).setVisible(true);
        menu.findItem(R.id.logout).setVisible(true);
        menu.findItem(R.id.signin).setVisible(false);
        menu.findItem(R.id.signup).setVisible(false);
        itemCart = menu.findItem(R.id.nav_cart);
        LayerDrawable icon = (LayerDrawable) itemCart.getIcon();*/
        //setBadgeCount(this, icon, "9");

        //final MenuItem menuItem = menu.findItem(R.id.nav_cart);

      /*  View actionView = MenuItemCompat.getActionView(menuItem);
        cartCounter = (TextView) actionView.findViewById(R.id.cart_badge);*/

        home.setVisibility(View.VISIBLE);
        Orders.setVisibility(View.VISIBLE);
        account.setVisibility(View.VISIBLE);
        cart.setVisibility(View.VISIBLE);
        // carttxtcartcounter.setVisibility(View.VISIBLE);
        Help.setVisibility(View.VISIBLE);
        Logout.setVisibility(View.VISIBLE);
        Sinin.setVisibility(View.GONE);
        Sinup.setVisibility(View.GONE);
        cartCounter = Integer.parseInt(MyApplication.readStringPref(PrefData.sizeofcart));
        cartCounter = Integer.valueOf(carttxtcartcounter.getText().toString());
        if (cartCounter != 0) {
            carttxtcartcounter.setVisibility(View.VISIBLE);
        }

    }

    private void hideMenuItems() {
       /* menu = navView.getMenu();
        menu.findItem(R.id.nav_Resturants).setVisible(true);
        menu.findItem(R.id.nav_PastOrder).setVisible(false);
        menu.findItem(R.id.nav_setting).setVisible(false);
        menu.findItem(R.id.nav_cart).setVisible(false);
        menu.findItem(R.id.Help).setVisible(false);
        menu.findItem(R.id.logout).setVisible(false);
        menu.findItem(R.id.signin).setVisible(true);
        menu.findItem(R.id.signup).setVisible(true);*/

        home.setVisibility(View.VISIBLE);
        Orders.setVisibility(View.GONE);
        account.setVisibility(View.GONE);
//        cart.setVisibility(View.GONE);
        carttxtcartcounter.setVisibility(View.GONE);
        Help.setVisibility(View.GONE);
        Logout.setVisibility(View.GONE);
        Sinin.setVisibility(View.VISIBLE);
        Sinup.setVisibility(View.VISIBLE);

        cartCounter = Integer.valueOf(carttxtcartcounter.getText().toString());
        if (cartCounter == 0) {
            carttxtcartcounter.setVisibility(View.INVISIBLE);
        }

    }

    private void initialize() {
        navigationView = findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        navUsername = headerView.findViewById(R.id.tv_navUsernamer);
        mobileNumber = headerView.findViewById(R.id.tv_mobiledrawer);

        home = headerView.findViewById(R.id.llHome);
        language = headerView.findViewById(R.id.language);
        Orders = headerView.findViewById(R.id.nav_PastOrder);
        account = headerView.findViewById(R.id.nav_setting);
        cart = headerView.findViewById(R.id.nav_cart);
        carttxtcartcounter = headerView.findViewById(R.id.tv_nav_cartt);
        try {
            cartCounter = Integer.parseInt(MyApplication.readStringPref(PrefData.sizeofcart));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        //  cartCounter = Integer.parseInt(carttxtcartcounter.getText().toString());
        if (cartCounter < 1) {
            carttxtcartcounter.setVisibility(View.INVISIBLE);

        }

        Help = headerView.findViewById(R.id.Help);
        Logout = headerView.findViewById(R.id.logout);
        Sinin = headerView.findViewById(R.id.signin);
        Sinup = headerView.findViewById(R.id.signup);

        if (Util.getLocale().equalsIgnoreCase("ar")) {
            ((ImageView) headerView.findViewById(R.id.iv_language_flag)).setImageResource(R.drawable.ic_uae);
        }


        imageViewprofile = headerView.findViewById(R.id.nav_profile_pic);
        recyclerView = findViewById(R.id.recyclerView);
        setActionOnDrawerItem();

        if (getIntent().getExtras() != null) {
            fromOrderConfirmation = getIntent().getStringExtra("fromConfirmation");
            guestLogin = getIntent().getStringExtra("guestLogin");
            key = getIntent().getBooleanExtra("key", false);
            if (key) {
                fromOrderConfirmation = "";
                guestLogin = "";
            }

        } else {
            fromOrderConfirmation = "";
            guestLogin = "";
        }


        MyApplication.writeStringPref(PrefData.guest_login, guestLogin);
        Log.i("tag46", "initialize: " + guestLogin);
        Log.i("tag46", "initialize: " + key);
    }

    private void setActionOnDrawerItem() {
        home.setOnClickListener(v -> {
            displayCaned(new FragmentMainScreen(), "main");
            drawerLayout.closeDrawers();
        });

        Orders.setOnClickListener(v -> {
            displayCaned(new Fragment_Orders(), "order");
            drawerLayout.closeDrawers();
        });

        account.setOnClickListener(v -> {
            displayCaned(new Fragment_Profile(), "profile");
            drawerLayout.closeDrawers();
        });

        cart.setOnClickListener(v -> {
            Fragment_Cart fragCart = new Fragment_Cart();
            Bundle bundle = new Bundle();
            bundle.putString("cart", "fromDrawer");
            fragCart.setArguments(bundle);
            displayCaned(fragCart, "cart");
            drawerLayout.closeDrawers();
        });

        Help.setOnClickListener(v -> {
            drawerLayout.closeDrawers();
            startActivity(new Intent(ControllerActivity.this, com.nav.appiqo.agriyas.views.activity.Help.class));

        });

        Logout.setOnClickListener(v -> {
            logOut();
            drawerLayout.closeDrawers();
        });

        Sinin.setOnClickListener(v -> {
            startActivity(new Intent(ControllerActivity.this, SignInActivity.class));
            finish();
            drawerLayout.closeDrawers();
        });

        Sinup.setOnClickListener(v -> {
            startActivity(new Intent(ControllerActivity.this, SignUpActivity.class));
            finish();
            drawerLayout.closeDrawers();
        });

        language.setOnClickListener(v -> {
           // String lang = Util.getLocale().equalsIgnoreCase("ar") ? "en" : "ar";
            String lang = sharedPreferences.getString(Locale_KeyValue, "");
            if (lang.equals("en")){
                lang = "ar";
            }else {
                lang = "en";

            }
            changeLocale(lang);

            startActivity(new Intent(ControllerActivity.this, LanguageChange.class));
            finish();
        });
    }

    //Save locale method in preferences
    public void saveLocale(String lang) {
        editor.putString(GettingStartFragment.Locale_KeyValue, lang);
        editor.apply();
        Log.e("print vlue lan save", lang);
    }

    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);//Set Selected Locale
        saveLocale(lang);//Save the selected locale


        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());//Update the config


//        updateViews();//Update texts according to locale
    }

    private void setDefaultValues() {
        if (!MyApplication.readStringPref(PrefData.User_userfile).equals("")) {
            Glide.with(ControllerActivity.this).load(MyApplication.readStringPref(PrefData.User_userfile)).apply(new RequestOptions().placeholder(R.drawable.profilepic)).into(imageViewprofile);
        }
        Log.i("tag46", "defaultvalues: " + guestLogin);


        try {

            if (guestLogin.equalsIgnoreCase("guestLogin")) {
                navUsername.setText(getResources().getString(R.string.sultan_ali_meqbali));
                mobileNumber.setText("");
            } else {

                navUsername.setText(MyApplication.readStringPref(PrefData.full_name));
                mobileNumber.setText(MyApplication.readStringPref(PrefData.user_mobile));
            }



        /*    if (guestLogin.equalsIgnoreCase("")) {
                navUsername.setText(MyApplication.readStringPref(PrefData.full_name));
                mobilenumber.setText(MyApplication.readStringPref(PrefData.user_mobile));
            } else if (guestLogin.equalsIgnoreCase("guestLogin")) {
                navUsername.setText("Welcome Guest");
                mobilenumber.setText("");
                Glide.with(ControllerActivity.this).load(R.drawable.profilepic).into(imageViewprofile);
            } else if (guestLogin.equalsIgnoreCase("signIn")) {
                navUsername.setText(MyApplication.readStringPref(PrefData.full_name));
                mobilenumber.setText(MyApplication.readStringPref(PrefData.user_mobile));
            } else if (guestLogin.equalsIgnoreCase("signUp")) {
                navUsername.setText(MyApplication.readStringPref(PrefData.full_name));
                mobilenumber.setText(MyApplication.readStringPref(PrefData.user_mobile));
            }*/
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Utils.Statusremove(ControllerActivity.this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.VISIBLE);
    }

    private void logOut() {
        MyApplication.writeBooleanPref(PrefData.LOGINSTATUS, false);
        MyApplication.clearPref();
        new PrefData(ControllerActivity.this).clear();
        Intent intent = new Intent(ControllerActivity.this, SignInActivity.class);
        intent.putExtra("type", "0");
        startActivity(intent);
        // startActivity(new Intent(ControllerActivity.this, SignInActivity.class));
        finish();
    }

    public void hideTool() {
        toolbar.setVisibility(View.GONE);
    }

    public void displayCaned(Fragment fragment, String s) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.framelayout, fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void chanzfrag(Fragment fragment, String s) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.framelayout, fragment);
        // ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();

        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void updatePic() {
        progressView.showLoader();
        AsyncHttpPost post = new AsyncHttpPost("http://bsmadvisers.com/api/jazzar/api/user/uploadUserProfileImage");
        MultipartFormDataBody body = new MultipartFormDataBody();
        body.addFilePart("image", file);
        body.addStringPart("user_id", MyApplication.readStringPref(PrefData.USER_ID) + "");
        body.addStringPart("lang", Util.getLocale());


        post.setBody(body);
        AsyncHttpClient.getDefaultInstance().executeString(post, new AsyncHttpClient.StringCallback() {
            @Override
            public void onCompleted(final Exception ex, AsyncHttpResponse source, final String result) {
                runOnUiThread(() -> {
                    if (ex != null) {
                        ex.printStackTrace();
                        return;
                    }
                    Log.v("Image", result);
                    try {
                        JSONObject jobject = new JSONObject(result);
                        final String messages = jobject.getString("message");
                        if (!jobject.getBoolean("error")) {
                            final String image = jobject.getString("data");


                            progressView.hideLoader();
                            MyApplication.writeStringPref(PrefData.User_userfile, image);
                            Glide.with(ControllerActivity.this).load(image).into(imageViewprofile);
                            Util.showSnackBar(drawerLayout, messages, ControllerActivity.this);


                        } else {
                            Util.showSnackBar(drawerLayout, messages, ControllerActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });

            }
        });
    }

    private void updatePicx() {
        Log.i("tag46", "updatePic: ;" + MyApplication.readStringPref(PrefData.User_userfile));
        progressView.showLoader();

        Call<UploadImageModel> call = apiInterface.uploadUserProfileImage(

                MyApplication.readStringPref(PrefData.USER_ID),
                attachPic(), Util.getLocale());

        call.enqueue(new Callback<UploadImageModel>() {
            @Override
            public void onResponse(Call<UploadImageModel> call, Response<UploadImageModel> response) {
                progressView.hideLoader();
                if (!response.body().isError()) {
                    Log.i("tag46", "onResponse moiimimk: " + response.body().getData());
                    MyApplication.writeStringPref(PrefData.User_userfile, response.body().getData());
                    Glide.with(ControllerActivity.this).load(response.body().getData()).into(imageViewprofile);

                    Util.showSnackBar(drawerLayout, response.body().getMessage(), ControllerActivity.this);
                } else {
                    Util.showSnackBar(drawerLayout, response.body().getMessage(), ControllerActivity.this);
                }
            }

            @Override
            public void onFailure(Call<UploadImageModel> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    private MultipartBody.Part attachPic() {
        if (file == null) {
            return null;
        } else {
            MultipartBody.Part profilePic = null;
            return profilePic = MultipartBody.Part.createFormData("image", "profilePicsas" + file + ".jpg", RequestBody.create(MediaType.parse("image/jpeg"), file));
        }
    }

    private void connectGetprofile() {
        progressView.showLoader();
        String user_id = MyApplication.readStringPref(PrefData.USER_ID);


        Call<GetProfile> call = apiInterface.getProfile(user_id, Util.getLocale());
        call.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                progressView.hideLoader();
                try {
                    if (!response.body().isError()) {
                        Log.i("tag46", "onResponse: connrectProfile" + response.body().getData().getImage());
                        MyApplication.writeStringPref(PrefData.User_userfile, response.body().getData().getImage());
                        MyApplication.writeStringPref(PrefData.USER_ID, response.body().getData().getUser_id());
                        MyApplication.writeStringPref(PrefData.user_mobile, response.body().getData().getMobile());
                        MyApplication.writeStringPref(PrefData.full_name, response.body().getData().getFull_name());
                        MyApplication.writeStringPref(PrefData.user_email, response.body().getData().getEmail());
                        MyApplication.writeStringPref(PrefData.user_mobile, response.body().getData().getMobile());
                    /*navUsername.setText(MyApplication.readStringPref(PrefData.full_name));
                    mobilenumber.setText(MyApplication.readStringPref(PrefData.user_mobile));*/

                    } else {
                        //   Util.showSnackBar(drawerLayout, response.body().getMessage(), ControllerActivity.this);
                    }

                } catch (Exception e) {
                }

            }

            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                t.printStackTrace();
                progressView.hideLoader();
            }
        });
    }

    public void drawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
            initializeCountDrawer();

        }
    }

    @Override
    public void onResume() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onResume();
        // displayCaned(new Fragment_Orders(), "order");


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            file = new File(images.get(0).getPath());
            Glide.with(ControllerActivity.this).load(images.get(0).getPath()).apply(new RequestOptions().placeholder(R.drawable.profilepic)).into(Fragment_Profile.profilepicedit);
            updatePic();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Fragment selectedFragment = getSupportFragmentManager().findFragmentById(R.id.framelayout);

        if (selectedFragment instanceof FragmentMainScreen) {
            finish();
        } else if (selectedFragment instanceof ItemDescriptionFragment) {
            selectedFragment.getChildFragmentManager().popBackStack("main", 0);
        } else if (selectedFragment instanceof Fragment_Cart) {
            //  chanzfrag(new Fragment_Mainscreen(),"main");
            if (!Fragment_Cart.CallingFragment.equalsIgnoreCase("fromItemDescription")) {
                chanzfrag(new FragmentMainScreen(), "main");
            } else {
                selectedFragment.getChildFragmentManager().popBackStack("itemDescription", 0);
                //  selectedFragment.getChildFragmentManager().popBackStack("main", 0);
            }
        } else if (selectedFragment instanceof MapFragment) {
            selectedFragment.getChildFragmentManager().popBackStack("fragCart", 0);
        } else if (selectedFragment instanceof FragmentConfimation) {
            displayCaned(new FragmentMainScreen(), "main");
            selectedFragment.getChildFragmentManager().popBackStack("confirm", 0);
        } else if (selectedFragment instanceof Fragment_Orders) {
            chanzfrag(new FragmentMainScreen(), "main");
            //  selectedFragment.getChildFragmentManager().popBackStack("", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void initializeCountDrawer() {

        carttxtcartcounter.setText(MyApplication.readStringPref(PrefData.sizeofcart));

        try {
            cartCounter = Integer.parseInt(carttxtcartcounter.getText().toString());
        } catch (NumberFormatException ex) { // handle your exception
        }

        if (cartCounter == 0) {
            carttxtcartcounter.setVisibility(View.INVISIBLE);

        } else
            carttxtcartcounter.setVisibility(View.VISIBLE);

    }

}