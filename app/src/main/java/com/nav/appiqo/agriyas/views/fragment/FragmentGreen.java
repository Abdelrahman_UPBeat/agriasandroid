package com.nav.appiqo.agriyas.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.views.activity.WalkThroughActivity;

public class FragmentGreen extends Fragment {


    View view;

    public static ImageView dotsGreen;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.view_green,container,false);

        dotsGreen=view.findViewById(R.id.iv_green);

        return view;
    }
}
