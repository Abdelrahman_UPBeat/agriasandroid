package com.nav.appiqo.agriyas.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nav.appiqo.agriyas.R;
//import com.example.appiqo.jazzar_meating.adapter.Fav_adapter;
import com.nav.appiqo.agriyas.models.Fav_modelclass;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/*
public class Fragment_favourite extends Fragment {
    @BindView(R.id.rv_fav)
    RecyclerView rvFav;
    Unbinder unbinder;
    List<Fav_modelclass> myOrderModelListfav;
    @BindView(R.id.iv_menutoolfav)
    ImageView ivMenutoolfav;
    @BindView(R.id.tv_toolbartiltlefav)
    TextView tvToolbartiltlefav;
    @BindView(R.id.toolbarfav)
    Toolbar toolbarfav;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favourite, container, false);
        unbinder = ButterKnife.bind(this, v);
        myOrderModelListfav = new ArrayList<>();


        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbarfav);
        toolbarfav.setVisibility(View.VISIBLE);


        ivMenutoolfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ControllerActivity) getActivity()).drawer();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFav.setLayoutManager(linearLayoutManager);
        Fav_adapter fav_adapter = new Fav_adapter(getContext(), myOrderModelListfav);
        rvFav.setAdapter(fav_adapter);
        prepreData();
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void prepreData() {
        myOrderModelListfav.add(new Fav_modelclass(R.drawable.meatpece, "Nasor King Sabel", "The palatable sensation we lovinly refer to as te ceerburer"));
        myOrderModelListfav.add(new Fav_modelclass(R.drawable.meatpece, "Nasor King Sabel", "The palatable sensation we lovinly refer to as te ceerburer"));
        myOrderModelListfav.add(new Fav_modelclass(R.drawable.meatpece, "Nasor King Sabel", "The palatable sensation we lovinly refer to as te ceerburer"));
        myOrderModelListfav.add(new Fav_modelclass(R.drawable.meatpece, "Nasor King Sabel", "The palatable sensation we lovinly refer to as te ceerburer"));
        myOrderModelListfav.add(new Fav_modelclass(R.drawable.meatpece, "Nasor King Sabel", "The palatable sensation we lovinly refer to as te ceerburer"));

    }
}
*/
