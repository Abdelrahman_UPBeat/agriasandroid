package com.nav.appiqo.agriyas.Utility;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {
    float downX;

    public CustomViewPager(@NonNull Context context) {
        super(context);
    }

    public CustomViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    private boolean wasSwipeToRightEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                return false;

            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                return downX - event.getX() > 0;

            default:
                return false;
        }
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean wasSwipeToRight = this.wasSwipeToRightEvent(event);
        // Do what you want here with left/right swipe
        if (wasSwipeToRight) {
            Log.e("right", "right");
        }


        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean wasSwipeToRight = this.wasSwipeToRightEvent(event);
        // Do what you want here with left/right swipe

        if (wasSwipeToRight) {
            Log.e("right", "right");
        }

        return super.onTouchEvent(event);
    }


}
