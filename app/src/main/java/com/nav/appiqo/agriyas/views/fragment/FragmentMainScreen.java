package com.nav.appiqo.agriyas.views.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.MyEdittext;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.HomePageModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.adapter.MyOrderAdapter;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.lhh.ptrrv.library.PullToRefreshRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMainScreen extends Fragment {

    public int NUM_OF_COLUMNS = 0;
    @BindView(R.id.imageViewProfile)
    ImageView imageViewProfile;
    @BindView(R.id.toolbar2)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.iv_searcicon)
    ImageView ivSearcicon;
    @BindView(R.id.iv_menuicon)
    ImageView ivMenuicon;
    @BindView(R.id.mainlayout)
    CoordinatorLayout mainlayout;
    @BindView(R.id.ptrrv)
    PullToRefreshRecyclerView recyclerviewMain;
    @BindView(R.id.search_bar)
    MyEdittext searchBar;
    List<HomePageModel.DataBean> myOrderModelList;
    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;
    MyOrderAdapter myOrderAdapter;
    Unbinder unbinder;
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mainscreen, container, false);
        unbinder = ButterKnife.bind(this, v);
        sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getActivity(), R.color.yourTranslucentColor)));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressView = new ProgressView(getContext());
        apiInterface = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiInterface.class);
        prefData = new PrefData(getActivity().getApplicationContext());
        myOrderModelList = new ArrayList<>();

        NUM_OF_COLUMNS = 2;
        recyclerviewMain.setSwipeEnable(true);

        recyclerviewMain.setLayoutManager(new GridLayoutManager(getActivity(), NUM_OF_COLUMNS));
        myOrderAdapter = new MyOrderAdapter(getActivity(), myOrderModelList);
        recyclerviewMain.setAdapter(myOrderAdapter);

        recyclerviewMain.setOnRefreshListener(() -> connectApiToGetSearchedData("0", "", 0));

        ((ControllerActivity) getActivity()).hideTool();
        ((ControllerActivity) getActivity()).hideKeyboard();
        Utils.Statusremove(getActivity());


        ivMenuicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ControllerActivity) getActivity()).drawer();

            }
        });


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                connectApiToGetSearchedData("1", s.toString(), 1);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        connectApiToGetSearchedData("1", "", 0);

        return v;
    }

    private String loadLocale() {
        return Locale.getDefault().getLanguage();

    }

    private void connectApiToGetSearchedData(final String status, String searchedItem, final int loader) {

        if (loader == 1) {
            //do nothing
        } else {
            progressView.showLoader();
        }


        Call<HomePageModel> call = apiInterface.homepage(
                "1",
                searchedItem,sharedPreferences.getString(Locale_KeyValue, "")
               );
/* Util.getLocale()*/
        call.enqueue(new Callback<HomePageModel>() {
            @Override
            public void onResponse(Call<HomePageModel> call, Response<HomePageModel> response) {
                if (loader == 1) {
                    //do nothing
                } else {
                    progressView.hideLoader();
                }

                try {


                    if (!response.body().isError()) {

                        myOrderModelList.clear();
                        myOrderModelList.addAll(response.body().getData());
                        myOrderAdapter.notifyDataSetChanged();

                        if (status.equalsIgnoreCase("0")) {
                            recyclerviewMain.setOnRefreshComplete();
                        }

                    } else {
                        Util.showSnackBar(mainlayout, response.body().getMessage(), getActivity());
                    }

                } catch (NullPointerException e) {

                }
            }

            @Override
            public void onFailure(Call<HomePageModel> call, Throwable t) {
                t.printStackTrace();
                if (loader == 1) {
                    //do nothing
                } else {
                    progressView.hideLoader();
                    if (NetworkUtil.isOnline(getActivity())) {
                        Toast.makeText(getActivity(), "Something gone wrong", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(getActivity(), "Bad Network", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}