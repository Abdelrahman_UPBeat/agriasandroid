package com.nav.appiqo.agriyas.WebApi;

import java.util.List;

public class GetMyCartModel {

    /**
     * error : false
     * error_code : 100
     * message : My Cart.
     * data : [{"item_id":"9","item_name":"BBQ box","image":"http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698814213b87d49-0912-4119-87e2-881b49d4e4b3.jpg","cart_id":"51","price":"300","quantity":"1"},{"item_id":"8","item_name":"Whole Lamb","image":"http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698811431028f74-23c3-46df-bcdb-a26780b3cec8.jpg","cart_id":"52","price":"250","quantity":"1"},{"item_id":"10","item_name":"Lamb Shoulder","image":"http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269882124dad8255-0807-4777-9c6a-dadb88eb3a0a.jpg","cart_id":"53","price":"300","quantity":"3"}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * item_id : 9
         * item_name : BBQ box
         * image : http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698814213b87d49-0912-4119-87e2-881b49d4e4b3.jpg
         * cart_id : 51
         * price : 300
         * quantity : 1
         */

        private String item_id;
        private String item_name;
        private String item_name_ar;

        public String getItem_name_ar() {
            return item_name_ar;
        }

        public void setItem_name_ar(String item_name_ar) {
            this.item_name_ar = item_name_ar;
        }

        private String image;
        private String cart_id;
        private String price;
        private String quantity;
        private String stock;

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCart_id() {
            return cart_id;
        }

        public void setCart_id(String cart_id) {
            this.cart_id = cart_id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }
}
