package com.nav.appiqo.agriyas.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.adapter.OrderItemsAdapter;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.models.OrderItemsModelClass;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderItemActivity extends AppCompatActivity {


    @BindView(R.id.rclrview_orderitems)
    RecyclerView rclrviewOrderitems;
    @BindView(R.id.orderitemlayout)
    LinearLayout orderitemlayout;

    List<OrderItemsModelClass.DataBean> myOrderItemList;
    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;
    OrderItemsAdapter orderItemsAdapter;

    @BindView(R.id.iv_toolbar_orderitems)
    ImageView ivToolbarOrderitems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);
        ButterKnife.bind(this);
      //  orderitemtoolbar.setBackgroundColor(getResources().getColor(R.color.resendemailred));
        //  orderitemtoolbar.setNavigationIcon(getResources().getDrawable(R.drawable.backarrowblack));
        //  orderitemtoolbar.setTitle("Items");


        myOrderItemList = new ArrayList<>();

        apiInterface = ApiClient.getClient(OrderItemActivity.this).create(ApiInterface.class);
        progressView = new ProgressView(OrderItemActivity.this);
        prefData = new PrefData(OrderItemActivity.this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderItemActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rclrviewOrderitems.setLayoutManager(linearLayoutManager);
        orderItemsAdapter = new OrderItemsAdapter(OrderItemActivity.this, myOrderItemList);
        rclrviewOrderitems.setAdapter(orderItemsAdapter);
        connectApiItemOrder();
    }

    private void connectApiItemOrder() {
        progressView.showLoader();

        String orderid = getIntent().getStringExtra("orderid");

        Call<OrderItemsModelClass> call = apiInterface.orderItems(orderid,Util.getLocale());
        call.enqueue(new Callback<OrderItemsModelClass>() {
            @Override
            public void onResponse(Call<OrderItemsModelClass> call, Response<OrderItemsModelClass> response) {
                progressView.hideLoader();

                if (!response.body().isError()) {

                    myOrderItemList.clear();
                    myOrderItemList.addAll(response.body().getData());

                    orderItemsAdapter.notifyDataSetChanged();


                } else {
                    Util.showSnackBar(orderitemlayout, response.body().getMessage(), OrderItemActivity.this);

                }
            }

            @Override
            public void onFailure(Call<OrderItemsModelClass> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.iv_toolbar_orderitems)
    public void onViewClicked() {
        onBackPressed();
    }
}
