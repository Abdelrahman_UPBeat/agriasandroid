package com.nav.appiqo.agriyas.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.views.activity.WalkThroughActivity;

public class FragmentBlue  extends Fragment {


    View view;
    public static ImageView dotsBlue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.view_blue,container,false);
        dotsBlue=view.findViewById(R.id.iv_blue);
        return view;
    }
}
