package com.nav.appiqo.agriyas.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.HomePageModel;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nav.appiqo.agriyas.views.fragment.ItemDescriptionFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<HomePageModel.DataBean> myOrderModelList;
    private ArrayList imagearrey = new ArrayList();
    private boolean en;
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private  SharedPreferences sharedPreferences;
    private  SharedPreferences.Editor editor;


    public MyOrderAdapter(Context contextorder, List<HomePageModel.DataBean> myOrderModelList ) {
        this.context = contextorder;
        this.myOrderModelList = myOrderModelList;
        sharedPreferences = context.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        this.en =  sharedPreferences.getString(Locale_KeyValue, "").equals("en");
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainscreen_rvlaout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder holder2 = (ViewHolder) holder;
        if(en){
            holder2.tvPartsName.setText(myOrderModelList.get(position).getItem_name());
        }else{
            holder2.tvPartsName.setText(myOrderModelList.get(position).getItem_name_ar());
        }


        Glide.with(context).load(myOrderModelList.get(position).getImages().get(0)).into(holder2.ivParts);
        holder2.tvPartsprice.setText("AED " + myOrderModelList.get(position).getPrice());
        holder2.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int stock = Integer.parseInt(myOrderModelList.get(position).getStock());
                if (stock < 1) {
                    Toast.makeText(context, R.string.out_of_stock, Toast.LENGTH_SHORT).show();
                } else {
                    ItemDescriptionFragment itemDescriptionFragment = new ItemDescriptionFragment();
                    Bundle bundle = new Bundle();
                    if(en) {
                        bundle.putString("decription", myOrderModelList.get(position).getDescription());
                        bundle.putString("itemname", myOrderModelList.get(position).getItem_name());
                    }else{
                        bundle.putString("decription", myOrderModelList.get(position).getDescription_ar());
                        bundle.putString("itemname", myOrderModelList.get(position).getItem_name_ar());
                    }

                    bundle.putString("itemprice", myOrderModelList.get(position).getPrice());

                    Log.i("tag46", "onClick: " + myOrderModelList.get(position).getImages());
                 /*   for (int i = 0; i < myOrderModelList.get(position).getImages().size(); i++) {
                        imagearrey[i]=myOrderModelList.get(position).getImages().get(i);
                    }*/
                    imagearrey.addAll(myOrderModelList.get(position).getImages());

                    //  bundle.("images",  myOrderModelList.get(position).getImages());

                    bundle.putStringArrayList("images", imagearrey);
                    bundle.putString("itemid", myOrderModelList.get(position).getItem_id());
                    bundle.putString("maxQuantity", myOrderModelList.get(position).getStock());
                    itemDescriptionFragment.setArguments(bundle);
                    ((ControllerActivity) context).displayCaned(itemDescriptionFragment, "itemDescription");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return myOrderModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_parts)
        ImageView ivParts;
        @BindView(R.id.tv_parts_name)
        TextView tvPartsName;
        @BindView(R.id.tv_partsprice)
        TextView tvPartsprice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}