package com.nav.appiqo.agriyas.views.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.CustomViewPager;
import com.nav.appiqo.agriyas.helper.CircleIndicator;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.views.fragment.FragmentBlue;
import com.nav.appiqo.agriyas.views.fragment.FragmentGreen;
import com.nav.appiqo.agriyas.views.fragment.FragmentRed;
import com.nav.appiqo.agriyas.views.fragment.GettingStartFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
//import me.relex.circleindicator.CircleIndicator;

public class WalkThroughActivity extends AppCompatActivity {

   public static WalkThroughActivity walkThroughActivity;
    CustomViewPager viewpager;
    CircleIndicator indicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_through);
        walkThroughActivity = this;
        ButterKnife.bind(this);
        viewpager = (CustomViewPager)findViewById(R.id.viewpager);
        indicator = (CircleIndicator)findViewById(R.id.indicator);
        if (MyApplication.readStringPref("first").equals("1")){
            viewpager.setAdapter(new MyPagerAdapterFirst(getSupportFragmentManager()));

        }else {
            viewpager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        }

        MyApplication.writeStringPref("first", "1");

        viewpager.setCurrentItem(0);
        indicator.setViewPager(viewpager);
        Statusremove();



    }


    public void Statusremove() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        }
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentGreen();

                case 1:
                    return new FragmentBlue();

                case 2:
                    return new FragmentRed();

                case 3:
                    return new GettingStartFragment();

                default:
                    break;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


    private class MyPagerAdapterFirst extends FragmentPagerAdapter {

        MyPagerAdapterFirst(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new GettingStartFragment();


                default:
                    break;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }


}
