package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.MyButton;
import com.nav.appiqo.agriyas.Utility.MyTextview;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.CrtOtpBean;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.WebApi.OtpResponseModel;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PinEntryEditText;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends AppCompatActivity {

    @BindView(R.id.iv_back_arrow)
    AppCompatImageView ivBackArrow;
    @BindView(R.id.text)
    MyTextview text;
    @BindView(R.id.tvPhoneNo)
    MyTextview tvPhoneNo;
    @BindView(R.id.ivEditNo)
    ImageView ivEditNo;
    @BindView(R.id.otp_view)
    PinEntryEditText otpView;
    @BindView(R.id.tvTimer)
    MyTextview tvTimer;
    @BindView(R.id.tvResendOtp)
    MyTextview tvResendOtp;
    @BindView(R.id.btnOtpSubmit)
    MyButton btnOtpSubmit;
    @BindView(R.id.root_otp)
    LinearLayout rootOtp;

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    CountDownTimer countDownTimer;
    Vibrator v;
    List<OtpResponseModel> data;

    String otp = "";
    String mobile = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        prefData = new PrefData(OtpActivity.this);
        progressView = new ProgressView(OtpActivity.this);
        apiInterface = ApiClient.getClient(OtpActivity.this).create(ApiInterface.class);

        otp = getIntent().getStringExtra("otp");
        mobile = getIntent().getStringExtra("mobile");
        data = new ArrayList<>();
        otpView.setText(otp);
        tvPhoneNo.setText(mobile);


        countDownTimer = new CountDownTimer(1000 * 60, 1000) {
            public void onTick(long millisUntilFinished) {
                tvTimer.setText("seconds remaining: " + millisUntilFinished / 1000 + "Sec.");
                tvResendOtp.setVisibility(View.INVISIBLE);
            }

            public void onFinish() {
                tvTimer.setVisibility(View.INVISIBLE);
                tvResendOtp.setVisibility(View.VISIBLE);
                otpView.setText("");
            }
        }.start();
    }

    @OnClick({R.id.iv_back_arrow, R.id.tvResendOtp, R.id.btnOtpSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back_arrow:
                onBackPressed();
                break;
            case R.id.tvResendOtp:
                getDataFromApicrtOtp();
                break;
            case R.id.btnOtpSubmit:
                getDataFromApi();
                break;
        }
    }

    private void getDataFromApi() {
        if (!otpView.getText().toString().equalsIgnoreCase(otp)) {
            otpView.setText("");
            Utils.showSnackBar(rootOtp, "Please enter correct otp", otpView, OtpActivity.this);
        } else {
            progressView.showLoader();

            Call<OtpResponseModel> call = apiInterface.otp(MyApplication.readStringPref(PrefData.user_email), Integer.parseInt(otpView.getText().toString()),
                    "Android", PrefData.PREF_DEVIC_ID, PrefData.PREF_FCM_TOK,Util.getLocale());
            call.enqueue(new Callback<OtpResponseModel>() {
                @Override
                public void onResponse(Call<OtpResponseModel> call, Response<OtpResponseModel> response) {
                    progressView.hideLoader();

                    if (!response.body().isError()) {

                        response.body().setData(response.body().getData());
                        MyApplication.writeStringPref(PrefData.USER_ID, response.body().getData().getUser_id());
                        MyApplication.writeStringPref(PrefData.PREF_DEVIC_ID, response.body().getData().getDevice_id());
                        MyApplication.writeStringPref(PrefData.full_name, response.body().getData().getFull_name());
                        MyApplication.writeStringPref(PrefData.user_mobile, response.body().getData().getMobile());
                        MyApplication.writeStringPref(PrefData.User_userfile, response.body().getData().getImage());
                        MyApplication.writeStringPref(PrefData.user_email, response.body().getData().getEmail());
                        MyApplication.writeStringPref(PrefData.deviceId,response.body().getData().getDevice_id());
                        MyApplication.writeStringPref(PrefData.securityToken,response.body().getData().getSecurity_token());

                        MyApplication.writeBooleanPref(PrefData.LOGINSTATUS, true);

                        try{
                            if (getIntent().getStringExtra("type").equals("1")){
                                Intent intent = new Intent(OtpActivity.this, ControllerActivity.class);
                                intent.putExtra("guestLogin","signUp");
                                intent.putExtra("type","1");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }



                        }catch (Exception e){
                            Intent intent = new Intent(OtpActivity.this, ControllerActivity.class);
                            intent.putExtra("guestLogin","signUp");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        }



                    } else {
                        Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OtpResponseModel> call, Throwable t) {
                    if ( NetworkUtil.isOnline(OtpActivity.this)){
                        Toast.makeText(OtpActivity.this,"Something gone wrong",Toast.LENGTH_SHORT ).show();
                    }else
                        Toast.makeText(OtpActivity.this,"Bad Network",Toast.LENGTH_SHORT ).show();
                    t.printStackTrace();
                }
            });
        }
    }

    private void getDataFromApicrtOtp() {
        if (!otpView.getText().toString().equalsIgnoreCase("")) {
            otpView.setText("");
            Utils.showSnackBar(rootOtp, "Otp sent to your mail Id", otpView, OtpActivity.this);
        } else {
            progressView.showLoader();

            Call<CrtOtpBean> call = apiInterface.createotp(MyApplication.readStringPref(PrefData.user_email), Util.getLocale());
            call.enqueue(new Callback<CrtOtpBean>() {
                @Override
                public void onResponse(Call<CrtOtpBean> call, Response<CrtOtpBean> response) {
                    progressView.hideLoader();

                    if (response.body().getError_code() == 100) {
                        Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CrtOtpBean> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

