package com.nav.appiqo.agriyas.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.models.OrderItemsModelClass;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<OrderItemsModelClass.DataBean> myOrderModelList;


    public OrderItemsAdapter(Context contextorder, List<OrderItemsModelClass.DataBean> myOrderModelList) {
        this.context = contextorder;
        this.myOrderModelList = myOrderModelList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderitems_rv_layout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder holder2 = (ViewHolder) holder;
        holder2.tvItemsName.setText(myOrderModelList.get(position).getItem_name());

        Glide.with(context).load(myOrderModelList.get(position).getImage()).into(holder2.ivOrderitemImage);
        holder2.tvOrderitemamount.setText("AED " + myOrderModelList.get(position).getTotal());
        holder2.tvItemsquantity.setText( "(x"+myOrderModelList.get(position).getQuantity()+")");

    }

    @Override
    public int getItemCount() {
        return myOrderModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_orderitem_image)
        ImageView ivOrderitemImage;
        @BindView(R.id.tv_ItemsName)
        TextView tvItemsName;
        @BindView(R.id.tv_Itemsquantity)
        TextView tvItemsquantity;
        @BindView(R.id.tv_orderitemamount)
        TextView tvOrderitemamount;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
