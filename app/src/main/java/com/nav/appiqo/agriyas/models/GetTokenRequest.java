package com.nav.appiqo.agriyas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohamed Usama on 10/10/2019.
 */
public class GetTokenRequest {
    @SerializedName("grant_type")
    @Expose
    private String client_credentials;


    public GetTokenRequest(String client_credentials) {
        this.client_credentials = client_credentials;

    }
}
