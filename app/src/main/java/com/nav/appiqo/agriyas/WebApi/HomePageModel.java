package com.nav.appiqo.agriyas.WebApi;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class HomePageModel implements Parcelable  {


    /**
     * error : false
     * error_code : 100
     * message : homepage data.
     * data : [{"item_id":"5","item_name":"Whole Lamb","item_name_ar":"eeeeeee","price":"100","stock":"25","description":"A Natural pasture sheep feeds on fresh grain grass to give you soft , smelling meat that reflects the quality  our meat","description_ar":"eeeeeeeeeeeeee","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269877943aa70174-8169-4cfa-a833-70ce216423d5.jpg","http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15281891233aa70174-8169-4cfa-a833-70ce216423d5.jpg"]},{"item_id":"6","item_name":"Lamb Chops","item_name_ar":"aaaaa","price":"300","stock":"0","description":"A Natural pasture sheep feeds on fresh grain grass to give you soft , smelling meat that reflects the quality of our meat","description_ar":"fresh","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269879050d62fa44-c6c8-442b-a258-35149d684349.jpg"]},{"item_id":"7","item_name":"Beaf Steak","item_name_ar":"","price":"500","stock":"43","description":"A Natural pasture sheep feeds on fresh grain grass to give you soft , smelling meat that reflects the quality of our meat","description_ar":"","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/1526988085ceec1ccf-dfb8-43d5-b158-d875f2d876a2.jpg"]},{"item_id":"8","item_name":"Whole Lamb","item_name_ar":"","price":"250","stock":"193","description":"A Natural pasture sheep feeds on fresh grain grass to give you soft , smelling meat that reflects the quality of our meat","description_ar":"","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698811431028f74-23c3-46df-bcdb-a26780b3cec8.jpg"]},{"item_id":"9","item_name":"BBQ box","item_name_ar":"","price":"300","stock":"437","description":" a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lor","description_ar":"","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/152698814213b87d49-0912-4119-87e2-881b49d4e4b3.jpg"]},{"item_id":"10","item_name":"Lamb Shoulder","item_name_ar":"","price":"300","stock":"271","description":" a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lor","description_ar":"","images":["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269882124dad8255-0807-4777-9c6a-dadb88eb3a0a.jpg"]}]
     */

    private boolean error;
    private int error_code;
    private String message;
    private List<DataBean> data;

    protected HomePageModel(Parcel in) {
        error = in.readByte() != 0;
        error_code = in.readInt();
        message = in.readString();
        data = in.createTypedArrayList(DataBean.CREATOR);
    }

    public static final Creator<HomePageModel> CREATOR = new Creator<HomePageModel>() {
        @Override
        public HomePageModel createFromParcel(Parcel in) {
            return new HomePageModel(in);
        }

        @Override
        public HomePageModel[] newArray(int size) {
            return new HomePageModel[size];
        }
    };

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (error ? 1 : 0));
        parcel.writeInt(error_code);
        parcel.writeString(message);
        parcel.writeTypedList(data);
    }

    public static class DataBean  implements Parcelable{
        /**
         * item_id : 5
         * item_name : Whole Lamb
         * item_name_ar : eeeeeee
         * price : 100
         * stock : 25
         * description : A Natural pasture sheep feeds on fresh grain grass to give you soft , smelling meat that reflects the quality  our meat
         * description_ar : eeeeeeeeeeeeee
         * images : ["http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15269877943aa70174-8169-4cfa-a833-70ce216423d5.jpg","http://www.bsmadvisers.com/api/jazzar/api/uploads/item/15281891233aa70174-8169-4cfa-a833-70ce216423d5.jpg"]
         */

        private String item_id;
        private String item_name;
        private String item_name_ar;
        private String price;
        private String stock;
        private String description;
        private String description_ar;
        private List<String> images;

        protected DataBean(Parcel in) {
            item_id = in.readString();
            item_name = in.readString();
            item_name_ar = in.readString();
            price = in.readString();
            stock = in.readString();
            description = in.readString();
            description_ar = in.readString();
            images = in.createStringArrayList();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_name_ar() {
            return item_name_ar;
        }

        public void setItem_name_ar(String item_name_ar) {
            this.item_name_ar = item_name_ar;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription_ar() {
            return description_ar;
        }

        public void setDescription_ar(String description_ar) {
            this.description_ar = description_ar;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(item_id);
            parcel.writeString(item_name);
            parcel.writeString(item_name_ar);
            parcel.writeString(price);
            parcel.writeString(stock);
            parcel.writeString(description);
            parcel.writeString(description_ar);
            parcel.writeStringList(images);
        }
    }
}
