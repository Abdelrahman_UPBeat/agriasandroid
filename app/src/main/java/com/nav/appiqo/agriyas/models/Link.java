package com.nav.appiqo.agriyas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Link {


    public PaymentP getPayment() {
        return payment;
    }

    public void setPayment(PaymentP payment) {
        this.payment = payment;
    }

    @SerializedName("payment")
    @Expose
    private PaymentP payment;
}
