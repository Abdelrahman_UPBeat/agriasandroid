package com.nav.appiqo.agriyas.views.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nav.appiqo.agriyas.views.activity.SignInActivity;
import com.nav.appiqo.agriyas.views.activity.SignUpActivity;
import com.nav.appiqo.agriyas.views.activity.SplashActivity;
import com.nav.appiqo.agriyas.views.activity.WalkThroughActivity;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class GettingStartFragment extends Fragment {


    @BindView(R.id.iv_dwnarrow)
    ImageView ivDwnarrow;
    @BindView(R.id.bt_signin)
    Button btSignin;
    @BindView(R.id.bt_signup)
    Button btSignup;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    @BindView(R.id.started)
    TextView started;

    private static Locale myLocale;

    View view;

    //Shared Preferences Variables
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    @BindView(R.id.simpleSpinner)
    Spinner simpleSpinner;

    String[] Languages = {"English(United states)", "Arabic"};
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_getting_start, container, false);
        unbinder = ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        final Spinner spin = view.findViewById(R.id.simpleSpinner);

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, Languages);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        if (sharedPreferences.getString(Locale_KeyValue, "").equalsIgnoreCase("en")) {
            spin.setSelection(0);
        } else {
            spin.setSelection(1);
        }

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemlang = parent.getItemAtPosition(position).toString();

                String lang;
                lang = "en";

                if (selectedItemlang == "English(United states)") {
                    Log.e("poss 0", lang);

                    lang = "en";
                    changeLocale(lang);
                    editor.putInt("last index", spin.getSelectedItemPosition()).apply();

                    Log.e("lan enlis locale", lang);

                }
                if (selectedItemlang == "Arabic") {
                    lang = "ar";
                    Log.e("poss 0", lang);
                    changeLocale(lang);
                    editor.putInt("last index", spin.getSelectedItemPosition()).apply();
                    Log.e("lan arbic locale", lang);
                }
                changeLocale(lang);


            }
            // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                intent.putExtra("type", "0");
                startActivity(intent);

            }
        });

        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);

            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ControllerActivity.class);
                intent.putExtra("guestLogin","guestLogin");
                startActivity(intent);
                //getActivity().finish();
            }
        });

        loadLocale();

        return view;
    }

    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);//Set Selected Locale
        saveLocale(lang);//Save the selected locale

        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());//Update the config
        updateViews();//Update texts according to locale

    }

    //Save locale method in preferences
    public void saveLocale(String lang) {
        editor.putString(Locale_KeyValue, lang);
        editor.apply();
        Log.e("print vlue lan save", lang);
    }

    //Get locale method in preferences
    public void loadLocale() {
        String language = sharedPreferences.getString(Locale_KeyValue, "");
        Log.e("lan load locale", language);
        changeLocale(language);
    }

    private void updateViews() {
        // Context context = LocaleHelper.setLocale(this, languageCode);
        // Resources resources = context.getResources();

        btSignin.setText((R.string.sign_In));
        btSignup.setText((R.string.sign_up_));
        tvSkip.setText((R.string.skip));
        started.setText((R.string.getting_started));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}