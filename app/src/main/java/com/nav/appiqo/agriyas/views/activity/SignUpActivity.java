package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.SessionManager;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.WebApi.SignUpResponse;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.helper.Validation;

import java.io.File;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.fullnameEditTextsinup)
    TextInputEditText fullnameEditTextsinup;
    @BindView(R.id.emailTextInputLayoutsinup)
    TextInputLayout emailTextInputLayoutsinup;
    @BindView(R.id.emailaddEditTextsinup)
    TextInputEditText emailaddEditTextsinup;
    @BindView(R.id.emailaddTextInputLayoutsinup)
    TextInputLayout emailaddTextInputLayoutsinup;
    @BindView(R.id.phoneyEditTextsinup)
    TextInputEditText phoneyEditTextsinup;
    @BindView(R.id.phoneTextInputLayoutsinup)
    TextInputLayout phoneTextInputLayoutsinup;
    @BindView(R.id.passwordEditTextsinup)
    TextInputEditText passwordEditTextsinup;
    @BindView(R.id.passwordTextInputLayoutsinup)
    TextInputLayout passwordTextInputLayoutsinup;
    @BindView(R.id.tv_signUp)
    TextView tvSignUp;
    @BindView(R.id.sinin)
    TextView sinin;


    Locale myLocale;
    ApiInterface apiInterface;
    ProgressView progressView;
    PrefData prefData;

    File file = null;
    @BindView(R.id.lineup_layout)
    LinearLayout lineupLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString("Locale_KeyValue", "");
       // changeLocale(name);

        apiInterface = ApiClient.getClient(SignUpActivity.this).create(ApiInterface.class);
        progressView = new ProgressView(SignUpActivity.this);
        prefData = new PrefData(SignUpActivity.this);


        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resources resources = getResources();

                if (Validation.nullValidator(fullnameEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.enterfullname), fullnameEditTextsinup, SignUpActivity.this);
                } else if (fullnameEditTextsinup.getText().toString().contains("  ")) {
                    fullnameEditTextsinup.setError(resources.getString(R.string.nomultiplespace));
                    // Utils.showSnackBar(lineupLayout, "No MultiSpaces Allowed", fullnameEditTextsinup, SignUpActivity.this);}
                } else if (Validation.nullValidator(emailaddEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.pleaeentermailid), emailaddEditTextsinup, SignUpActivity.this);
                } else if (!Validation.emailValidator(emailaddEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.PleaseEnterValidEmailID), emailaddEditTextsinup, SignUpActivity.this);
                } else if (Validation.nullValidator(phoneyEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.pleaseenterphonenumber), phoneyEditTextsinup, SignUpActivity.this);
                } else if (!Validation.isValidMobile(phoneyEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.pleaseenterphonenumberlessthen), phoneyEditTextsinup, SignUpActivity.this);
                } else if (phoneyEditTextsinup.getText().toString().contains("  ")) {
                    phoneyEditTextsinup.setError(resources.getString(R.string.nomultiplespace));
                } else if (Validation.nullValidator(passwordEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.PleaseEnterpassword), passwordEditTextsinup, SignUpActivity.this);
                } else if (!Validation.passValidator(passwordEditTextsinup.getText().toString())) {
                    Utils.showSnackBar(lineupLayout, resources.getString(R.string.passwordmust6character), passwordEditTextsinup, SignUpActivity.this);
                } else {
                    connectApiSignup();
                }
                // startActivity(new Intent(SignUpActivity.this, ControllerActivity.class).putExtra("mobile", phoneyEditTextsinup.getText().toString()));

            }
        });

        sinin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });
    }


    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase("")) {
            Log.e("lan", "changeLocale: not");
            return;
        }

        myLocale = new Locale(lang);
        Log.e("lan", "changeLocale1: ");//Set Selected Locale

        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//Update the config
        updateViews();//Update texts according to locale
    }


    private void updateViews() {
        // Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = getResources();

        fullnameEditTextsinup.setText(resources.getString(R.string.sign_up));
        emailaddEditTextsinup.setText(resources.getString(R.string.email_address));
        //countryEditTextsinup.setText(resources.getString(R.string.country));
        phoneyEditTextsinup.setText(resources.getString(R.string.phone_number));
        passwordEditTextsinup.setText(resources.getString(R.string.password));
        passwordEditTextsinup.setText(resources.getString(R.string.password));
        tvSignUp.setText(resources.getString(R.string.sign_up));
        sinin.setText(resources.getString(R.string.sign_in));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void connectApiSignup() {

        String fullname = fullnameEditTextsinup.getText().toString();
        MyApplication.writeStringPref(PrefData.full_name, fullname);
        String emailaddress = emailaddEditTextsinup.getText().toString();
        String password = passwordEditTextsinup.getText().toString();
        String country = "UAE";
        String mobile = phoneyEditTextsinup.getText().toString();

        progressView.showLoader();

        Call<SignUpResponse> call = apiInterface.signup(fullname, emailaddress, password, country, mobile, MyApplication.readStringPref(PrefData.DEVICE_ID), Util.getLocale());
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                progressView.hideLoader();
                try {
                    if (response.body().getError_code() == 100) {

                        Toast.makeText(SignUpActivity.this, "Successfully", Toast.LENGTH_SHORT).show();
                        MyApplication.writeStringPref(PrefData.user_email, response.body().getData().getEmail());

                        String otp = response.body().getData().getOtp();
                        try{

                            if (getIntent().getStringExtra("type").equals("1")){
                                startActivity(new Intent(SignUpActivity.this, OtpActivity.class).putExtra("type", "1").putExtra("otp", otp).putExtra("mobile", phoneyEditTextsinup.getText().toString()));
                            }else {
                                startActivity(new Intent(SignUpActivity.this, OtpActivity.class).putExtra("otp", otp).putExtra("mobile", phoneyEditTextsinup.getText().toString()));

                            }
                        }catch (Exception e)
                        {
                            startActivity(new Intent(SignUpActivity.this, OtpActivity.class).putExtra("otp", otp).putExtra("mobile", phoneyEditTextsinup.getText().toString()));

                        }

                    } else {
                        Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                progressView.hideLoader();
                if (NetworkUtil.isOnline(SignUpActivity.this)) {
                    Toast.makeText(SignUpActivity.this, "Something gone wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(SignUpActivity.this, "Bad Network", Toast.LENGTH_SHORT).show();
                t.printStackTrace();


            }
        });
    }

    private MultipartBody.Part attachpic() {
        if (file == null) {
            return null;
        } else {
            MultipartBody.Part profilePic = null;
            return profilePic = MultipartBody.Part.createFormData("userfile", "profilePic" + MyApplication.readStringPref(PrefData.User_userfile) + ".jpg", RequestBody.create(MediaType.parse("image/jpeg"), file));
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            if (images.size() > 0) {
                file = new File(images.get(0).getPath());
                Glide.with(SignUpActivity.this).load(images.get(0).getPath()).into(ivSignupProfilePic);
            } else {
                Utils.showSnackBar(lineupLayout, "Image not found", SignUpActivity.this);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);  // THIS METHOD SHOULD BE HERE so that ImagePicker works with fragment
    }*/

    public MultipartBody.Part checkPart(String path, String key) {
        MultipartBody.Part filePart;
        if (!path.equalsIgnoreCase("") && !path.equalsIgnoreCase("No file chosen")) {
            file = new File(path);
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            filePart = MultipartBody.Part.createFormData(key, file.getName(), requestBody);
        } else {
            filePart = null;
        }
        return filePart;
    }

}
