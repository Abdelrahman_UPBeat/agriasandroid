package com.nav.appiqo.agriyas.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.DeleteCartModel;
import com.nav.appiqo.agriyas.WebApi.GetMyCartModel;
import com.nav.appiqo.agriyas.WebApi.UpdateCartModel;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nav.appiqo.agriyas.views.fragment.Fragment_Cart;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static Integer refreshamount;
    public Fragment_Cart fragment_cart;
    int total = 0;
    private Context contextordercart;
    private List<GetMyCartModel.DataBean> myOrderModelListcart;
    private int i;
    private ProgressView progressView;
    private ApiInterface apiInterface;
    private boolean en;
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;


    public CartAdapter(Context contextordercart, List<GetMyCartModel.DataBean> myOrderModelListcart, Fragment_Cart fragment_cart ) {
        this.contextordercart = contextordercart;
        this.myOrderModelListcart = myOrderModelListcart;
        this.fragment_cart = fragment_cart;
        progressView = new ProgressView(contextordercart);
        apiInterface = ApiClient.getClient(contextordercart).create(ApiInterface.class);
        /*this.en =   Util.getLocale().equals("en");*/

        sharedPreferences = contextordercart.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        this.en =   sharedPreferences.getString(Locale_KeyValue, "").equals("en");
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_adapter_cart, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        final ViewHolder holder2 = (ViewHolder) holder;


        final String cartId = myOrderModelListcart.get(position).getCart_id();
        Log.i("amaount", "onClick:top cart " + cartId);
        if(en){
            holder2.tvCartitemname.setText(myOrderModelListcart.get(position).getItem_name());
        }else{
            holder2.tvCartitemname.setText(myOrderModelListcart.get(position).getItem_name_ar());
        }

        holder2.tvCartitemprice.setText("AED " + myOrderModelListcart.get(position).getPrice());
        refreshamount = Integer.parseInt(myOrderModelListcart.get(position).getQuantity());
        // holder2.ivCartdd.setImageResource(myOrderModelListcart.get(position).getAddimg());
        Glide.with(contextordercart).load(myOrderModelListcart.get(position).getImage()).into(((holder2.ivCartrim)));
        holder2.tvItemnocart.setText(String.valueOf(refreshamount));
        // holder2.ivCartminus.setImageResource(myOrderModelListcart.get(position).getMinusimg());
        Log.i("amaount", "onClick: older" + refreshamount);

        totalAmount();

       /* holder2.ivCartdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = Integer.parseInt(holder2.tvItemnocart.getText().toString());
                if (Integer.parseInt(myOrderModelListcart.get(position).getStock()) < 1) {
                    Toast.makeText(contextordercart, "This Item is Out Of Stock", Toast.LENGTH_SHORT).show();
                    return;
                }
                i++;
                refreshamount = i;
                String s = Integer.toString(refreshamount);
                myOrderModelListcart.get(position).setQuantity(String.valueOf(refreshamount));

                //   totalAmount(refreshamount,myOrderModelListcart.get(position).getPrice());
                //refreshamount = Integer.parseInt(myOrderModelListcart.get(position).getQuantity())+  Integer.parseInt(myOrderModelListcart.get(position).getPrice());
                holder2.tvItemnocart.setText(s);
                Log.i("amaount", "onClick: add" + refreshamount);
                totalAmount();
                Integer amount = Integer.parseInt(holder2.tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(position).getPrice()));
                holder2.tvTotalamount.setText("AED " + amount);

                //String itemId, String type, String quantity, final int pos

                connectApiUpdateMyCart(myOrderModelListcart.get(position).getItem_id(),
                        "add",myOrderModelListcart.get(position).getCart_id(), position);

            }
        });
        holder2.ivCartminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = Integer.parseInt(holder2.tvItemnocart.getText().toString());
                if (i > 0) {

                    try {
                        i--;
                        refreshamount = i;
                        String t = Integer.toString(refreshamount);
                        refreshamount = i;
                        myOrderModelListcart.get(position).setQuantity(String.valueOf(refreshamount));
                        Log.i("amaount", "onClick:minus " + refreshamount);

                        // refreshamount = Integer.parseInt(myOrderModelListcart.get(position).getQuantity())-  Integer.parseInt(myOrderModelListcart.get(position).getPrice());
                        holder2.tvItemnocart.setText(t);
                        totalAmount();
                        Integer amount = Integer.parseInt(holder2.tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(position).getPrice()));
                        holder2.tvTotalamount.setText("AED " + amount);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                    if (i == 0) {
                        connectApiDeleteMyCart(cartId, position);
                    } else {
                        connectApiUpdateMyCart(myOrderModelListcart.get(position).getItem_id(),
                                "remove",myOrderModelListcart.get(position).getCart_id(), position);
                    }
                }

            }
        });*/

        Integer amount = Integer.parseInt(holder2.tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(position).getPrice()));
        holder2.tvTotalamount.setText("AED " + amount);


        holder2.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent = new Intent(contextordercart, ItemDescpActivity.class);
                contextordercart.startActivity(intent);*/
            }
        });
    }

    private void totalAmount() {
        total = 0;
        Log.i("amaount", "onClick:top total " + refreshamount);

        for (GetMyCartModel.DataBean dataBean : myOrderModelListcart) {
            int quantity = Integer.parseInt(dataBean.getQuantity());
            int price = Integer.parseInt(dataBean.getPrice());
            total += quantity * price;
            Log.i("amaount", "onClick:quantity " + quantity);
            Log.i("amaount", "onClick: prce" + price);


        }
        Log.i("amaount", "onClick:total " + total);

        fragment_cart.swipeBtn.setText(contextordercart.getResources().getString(R.string.slide_to_pay)
                +" " + String.valueOf(total));
    }


    private void connectApiDeleteMyCart(String cartid, final int pos) {
        progressView.showLoader();

        String userId  ;
        if(ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")){
            userId = MyApplication.readStringPref(PrefData.DEVICE_ID);
        }else{
            userId = MyApplication.readStringPref(PrefData.USER_ID);
        }
        Log.i("cart", "connectApiDeleteMyCart: " + cartid);
        Call<DeleteCartModel> call = apiInterface.deleteCart(
                userId,
                cartid,Util.getLocale()
        );
        call.enqueue(new Callback<DeleteCartModel>() {
            @Override
            public void onResponse(Call<DeleteCartModel> call, Response<DeleteCartModel> response) {
                progressView.hideLoader();
                if (!response.body().isError()) {
                    Log.e("yipeee", "yipeeee");

                    myOrderModelListcart.remove(pos);
                    notifyItemRemoved(pos);
//                    notifyDataSetChanged();
                    String sizeofcart = String.valueOf(myOrderModelListcart.size());
                    MyApplication.writeStringPref(PrefData.sizeofcart, sizeofcart);

                    if (myOrderModelListcart.size() == 0) {
                        fragment_cart.swipeBtn.setText(contextordercart.getString(R.string.please_add_to_continue));

                        fragment_cart.swipeBtn.setVisibility(View.GONE);
                    }else {

                        fragment_cart.swipeBtn.setVisibility(View.VISIBLE);
                    }


                    Toast.makeText(contextordercart, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("yipeee", "oppppssssss");
                    Toast.makeText(contextordercart, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteCartModel> call, Throwable t) {
                t.printStackTrace();
                Log.e("yipeee", "shitttttt");
                progressView.hideLoader();
            }
        });
    }


    private UpdateCartModel.DataBean getPositionOfItem(List<UpdateCartModel.DataBean> newList,String cartId){
        for (int j = 0; j < newList.size(); j++) {
            if(newList.get(j).getCart_id().equals(cartId)){
                return newList.get(j);
            }
        }
        return null;

    }

    private void connectApiUpdateMyCart(String itemId, String type, final String cartId, final int pos) {
        progressView.showLoader();

        String userId  ;
        if(ControllerActivity.guestLogin.equalsIgnoreCase("guestLogin")){
            userId = MyApplication.readStringPref(PrefData.DEVICE_ID);
        }else{
            userId = MyApplication.readStringPref(PrefData.USER_ID);
        }

        Call<UpdateCartModel> call = apiInterface.updateCart(
                userId,
                itemId, type, "1",Util.getLocale()
        );
        call.enqueue(new Callback<UpdateCartModel>() {
            @Override
            public void onResponse(Call<UpdateCartModel> call, Response<UpdateCartModel> response) {
                progressView.hideLoader();
                UpdateCartModel body = response.body();
                if (body != null) {
                    if (!body.isError()) {
                        UpdateCartModel.DataBean responseData = getPositionOfItem(body.getData(),cartId) ;
                        if(responseData != null){
                            myOrderModelListcart.get(pos).setQuantity(responseData.getQuantity());
                            myOrderModelListcart.get(pos).setPrice(responseData.getPrice());
                            myOrderModelListcart.get(pos).setStock(responseData.getStock());

                            notifyItemChanged(pos);
                            String sizeOfCart = String.valueOf(myOrderModelListcart.size());
                            MyApplication.writeStringPref(PrefData.sizeofcart, sizeOfCart);

                            if (myOrderModelListcart.size() == 0) {
                                fragment_cart.swipeBtn.setText(contextordercart.getString(R.string.please_add_to_continue));
                            }


                         //   Toast.makeText(contextordercart, body.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e("yipeee", "oppppssssss");
                       // Toast.makeText(contextordercart, body.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(contextordercart, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UpdateCartModel> call, Throwable t) {
                t.printStackTrace();
                Log.e("yipeee", "shitttttt");
                progressView.hideLoader();
            }
        });
    }

    @Override
    public int getItemCount() {

        return myOrderModelListcart.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cartrim)
        ImageView ivCartrim;
        @BindView(R.id.tv_cartitemname)
        TextView tvCartitemname;
        @BindView(R.id.tv_cartitemprice)
        TextView tvCartitemprice;
        @BindView(R.id.iv_cartdd)
        ImageView ivCartdd;
        @BindView(R.id.tv_itemnocart)
        TextView tvItemnocart;
        @BindView(R.id.iv_cartminus)
        ImageView ivCartminus;
        @BindView(R.id.tv_totalamount)
        TextView tvTotalamount;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ivCartdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    i = Integer.parseInt( tvItemnocart.getText().toString());
                    if (Integer.parseInt(myOrderModelListcart.get(getAdapterPosition()).getStock()) < 1) {
                        Toast.makeText(contextordercart, R.string.out_of_stock, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    i++;
                    refreshamount = i;
                    String s = Integer.toString(refreshamount);
                    myOrderModelListcart.get(getAdapterPosition()).setQuantity(String.valueOf(refreshamount));

                    //   totalAmount(refreshamount,myOrderModelListcart.get(position).getPrice());
                    //refreshamount = Integer.parseInt(myOrderModelListcart.get(position).getQuantity())+  Integer.parseInt(myOrderModelListcart.get(position).getPrice());
                    tvItemnocart.setText(s);
                    Log.i("amaount", "onClick: add" + refreshamount);
                    totalAmount();
                    Integer amount = Integer.parseInt( tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(getAdapterPosition()).getPrice()));
                     tvTotalamount.setText("AED " + amount);

                    //String itemId, String type, String quantity, final int pos

                    connectApiUpdateMyCart(myOrderModelListcart.get(getAdapterPosition()).getItem_id(),
                            "add",myOrderModelListcart.get(getAdapterPosition()).getCart_id(), getAdapterPosition());

                }
            });
            ivCartminus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    i = Integer.parseInt( tvItemnocart.getText().toString());
                    if (i > 0) {

                        try {
                            i--;
                            refreshamount = i;
                            String t = Integer.toString(refreshamount);
                            refreshamount = i;
                            myOrderModelListcart.get(getAdapterPosition()).setQuantity(String.valueOf(refreshamount));
                            Log.i("amaount", "onClick:minus " + refreshamount);

                            // refreshamount = Integer.parseInt(myOrderModelListcart.get(position).getQuantity())-  Integer.parseInt(myOrderModelListcart.get(position).getPrice());
                            tvItemnocart.setText(t);
                            totalAmount();
                            Integer amount = Integer.parseInt( tvItemnocart.getText().toString()) * (Integer.parseInt(myOrderModelListcart.get(getAdapterPosition()).getPrice()));
                            tvTotalamount.setText("AED " + amount);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }


                        if (i == 0) {
                            connectApiDeleteMyCart(myOrderModelListcart.get(getAdapterPosition()).getCart_id(), getAdapterPosition());
                        } else {
                            connectApiUpdateMyCart(myOrderModelListcart.get(getAdapterPosition()).getItem_id(),
                                    "remove",myOrderModelListcart.get(getAdapterPosition()).getCart_id(), getAdapterPosition());
                        }
                    }

                }
            });
        }
    }
}
