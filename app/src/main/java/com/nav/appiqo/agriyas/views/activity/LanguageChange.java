package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.nav.appiqo.agriyas.ProgressBarAnimation;
import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;

public class LanguageChange extends AppCompatActivity {

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_lang);

        ProgressBar process = findViewById(R.id.progressbar1);

        ProgressBarAnimation anim = new ProgressBarAnimation(process, 0, 100);
        anim.setDuration(3000);
        process.startAnimation(anim);


        screentimer();
    }


    public void screentimer() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

              /*  if (MyApplication.readBooleanPref(PrefData.LOGINSTATUS)) {
                    startActivity(new Intent(LanguageChange.this, ControllerActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(LanguageChange.this, WalkThroughActivity.class));
                    finish();
                }*/
                startActivity(new Intent(LanguageChange.this, ControllerActivity.class));
                finish();
            }
        }, 3000);
    }


}