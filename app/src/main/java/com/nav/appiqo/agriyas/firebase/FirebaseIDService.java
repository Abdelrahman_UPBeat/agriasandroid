package com.nav.appiqo.agriyas.firebase;


import android.content.Intent;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.nav.appiqo.agriyas.SessionManager;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        PrefData prefData= new PrefData(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("token",refreshedToken);
        prefData.setPrefFcmTok(refreshedToken);
        Log.i( "onTokenRefresh: ",prefData.getPrefFcmTok().toString());
     //   prefData.setPrefFcmTok(refreshedToken);s

        MyApplication.writeStringPref(PrefData.PREF_FCM_TOK,refreshedToken);
        Log.i( "onTokenRefresh: ",prefData.getPrefFcmTok().toString());

        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        MyApplication.writeStringPref(PrefData.PREF_DEVIC_ID,android_id);
       // SessionManager.setFcmKey(this,refreshedToken);
    }
}