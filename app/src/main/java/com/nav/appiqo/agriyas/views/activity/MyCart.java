package com.nav.appiqo.agriyas.views.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.GetMyCartModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.adapter.CartAdapter;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.SwipeButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import payment.sdk.android.cardpayment.CardPaymentData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCart extends AppCompatActivity {
    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;
    public Boolean flat = true;

    CartAdapter cartAdapter;
    List<GetMyCartModel.DataBean> myOrderModelListcart;
    public static String CallingFragment = "";

    int totalAmount;
    JSONObject paramObject;
    JSONArray jsonArray;
    @BindView(R.id.floatingbutton)
    FloatingActionButton floatingbutton;
    @BindView(R.id.iv_menutoolcart)
    ImageView ivMenutoolcart;
    @BindView(R.id.tv_toolbartiltlecart)
    TextView tvToolbartiltlecart;
    @BindView(R.id.toolbarcart)
    Toolbar toolbarcart;
    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    @BindView(R.id.swipe_btn)
    SwipeButton swipeBtn;
    @BindView(R.id.cartmain)
    LinearLayout cartmain;
    private int i;
    int refreshamount = 0;
    int ACCESS_FINE_LOCATION_INTENT_ID = 101;
    public int finalAmountOfOrder = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        ButterKnife.bind(this);

        myOrderModelListcart = new ArrayList<>();
        progressView = new ProgressView(MyCart.this);
        apiInterface = ApiClient.getClient(MyCart.this.getApplicationContext()).create(ApiInterface.class);
     //   prefData = new PrefData(MyCart.this.getApplicationContext());





        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyCart.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCart.setLayoutManager(linearLayoutManager);
      //  cartAdapter = new CartAdapter(MyCart.this, myOrderModelListcart );
        rvCart.setAdapter(cartAdapter);

        connectApiGetMyCart();

    }

    public void connectApiGetMyCart() {
        progressView.showLoader();

        String userid = MyApplication.readStringPref(PrefData.USER_ID);
        Log.i("user", "connectApiGetMyCart: " + userid);
        Call<GetMyCartModel> call = apiInterface.getMyCart(userid,Util.getLocale());
        call.enqueue(new Callback<GetMyCartModel>() {
            @Override
            public void onResponse(Call<GetMyCartModel> call, Response<GetMyCartModel> response) {
                progressView.hideLoader();

                if (!response.body().isError()) {
                    myOrderModelListcart.clear();
                    finalAmountOfOrder = 0;
                    myOrderModelListcart.addAll(response.body().getData());

                    String sizeofcart = String.valueOf(myOrderModelListcart.size());
                    cartAdapter.notifyDataSetChanged();
                    MyApplication.writeStringPref(PrefData.sizeofcart, sizeofcart);
                  //  swipedisable();

                    for (GetMyCartModel.DataBean dataBean : myOrderModelListcart) {
                        int total = Integer.parseInt(dataBean.getPrice()) * Integer.parseInt(dataBean.getQuantity());
                        finalAmountOfOrder = finalAmountOfOrder + total;

                    }
                    Log.i("amount", "sefbskjdv+" + finalAmountOfOrder);
                    Log.i("amount", "sefbskjdv+" + finalAmountOfOrder);


                    if (myOrderModelListcart.size() == 0) {
                        swipeBtn.setText(getResources().getString(R.string.addtocart));
                    } else {
                        swipeBtn.setText(getResources().getString(R.string.slide_to_pay) + String.valueOf(finalAmountOfOrder));

                    }

                    if (myOrderModelListcart.size() == 0) {
                        Toast.makeText(MyCart.this, getResources().getString(R.string.cart_is_empty), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Util.showSnackBar(cartmain, response.body().getMessage(), MyCart.this);

                }
            }

            @Override
            public void onFailure(Call<GetMyCartModel> call, Throwable t) {
                if (NetworkUtil.isOnline(MyCart.this)) {
                    Toast.makeText(MyCart.this, "Something gone wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(MyCart.this, "Bad Network", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }
    private void passData() {
        if (myOrderModelListcart.size() == 0) {
            Util.showSnackBar(cartmain, getResources().getString(R.string.additemtocart), MyCart.this);
        } else {
            finalAmountOfOrder = 0;
            jsonArray = new JSONArray();
            try {
                int total = 0;
                for (int i = 0; i < myOrderModelListcart.size(); i++) {
                    paramObject = new JSONObject();
                    paramObject.put("item_id", myOrderModelListcart.get(i).getItem_id());
                    paramObject.put("quantity", myOrderModelListcart.get(i).getQuantity());
                    paramObject.put("price", myOrderModelListcart.get(i).getPrice());
                    totalAmount = Integer.parseInt(myOrderModelListcart.get(i).getQuantity()) * Integer.parseInt(myOrderModelListcart.get(i).getPrice());
                    total = totalAmount + total;
                    paramObject.put("total", String.valueOf(totalAmount));
                    finalAmountOfOrder = finalAmountOfOrder + totalAmount;
                    jsonArray.put(paramObject);


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("jsaonData", jsonArray.toString());

            // MapFragment mapFragment = new MapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("jsaonData", jsonArray.toString());
            bundle.putString("finalAmountOfOrder", "" + finalAmountOfOrder);

            requestLocationPermission();

          /*  if (locationpermission) {
                startActivity(new Intent(getActivity(), MapActivity.class).putExtra("bundle", bundle));

            } else {
                startActivity(new Intent(getActivity(), MapActivity.class).putExtra("bundle", bundle));

                Toast.makeText(getContext(), "Please grant the location permission", Toast.LENGTH_SHORT).show();
            }
*/
            //
            //mapFragment.setArguments(bundle);

            // ((ControllerActivity) getActivity()).displayCaned(mapFragment, "map");
        }
    }

    private void requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(MyCart.this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            passData();
        } else {
            ActivityCompat.requestPermissions(MyCart.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("tag46", "onRequestPermissionsResult: "+"dknjk");
        switch (requestCode) {
            case 101: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("tag46", "onRequestPermissionsResult: "+"dknjk");
                }else {

                }
            }
        }
/*   @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("tag46", "onRequestPermissionsResult: "+"dknjk");
        switch (requestCode) {
            case 101: {
                Log.i("tag46", "onRequestPermissionsResult: "+"dknjk");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //     locationpermission = true;
                    //If permission granted show location dialog if APIClient is not null
                   *//* if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();*//*
                    Log.i("tag46", "onRequestPermissionsResult: "+"dknjk");


                } else {
                    //  updateGPSStatus("Location Permission denied.");
                    Toast.makeText(getContext(), "Location Permission denied.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    locationpermission = false;
                }
                return;
            }
        }
    }*/


    }








}

