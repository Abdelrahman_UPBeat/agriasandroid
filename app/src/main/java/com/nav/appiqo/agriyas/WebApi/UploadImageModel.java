package com.nav.appiqo.agriyas.WebApi;

public class UploadImageModel {
    /**
     * error : false
     * error_code : 100
     * message : Image uploded
     * data : http://localhost/api/mfa/api/uploads/user/dnowndwdlllodaooalla1526925368.jpg
     */

    private boolean error;
    private int error_code;
    private String message;
    private String data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
