package com.nav.appiqo.agriyas.WebApi;

public class OtpResponseModel {


    /**
     * error : false
     * error_code : 100
     * message : User Verified Successfully
     * data : {"user_id":"37","security_token":"f1d4d4sfsd212ssdsf13","device_id":"fdfds3434122","full_name":"aman kumar","mobile":"9889778886","image":" ","email":"aaad@mail.com","status":"1"}
     */

    private boolean error;
    private int error_code;
    private String message;
    private DataBean data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 37
         * security_token : f1d4d4sfsd212ssdsf13
         * device_id : fdfds3434122
         * full_name : aman kumar
         * mobile : 9889778886
         * image :
         * email : aaad@mail.com
         * status : 1
         */

        private String user_id;
        private String security_token;
        private String device_id;
        private String full_name;
        private String mobile;
        private String image;
        private String email;
        private String status;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getSecurity_token() {
            return security_token;
        }

        public void setSecurity_token(String security_token) {
            this.security_token = security_token;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
