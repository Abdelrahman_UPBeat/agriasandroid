package com.nav.appiqo.agriyas.models;

public class Order_rvModel {

    public Order_rvModel(int oimage, String oitem, String oitemprice, String oitemdescp) {
        this.oimage = oimage;
        this.oitem = oitem;
        this.oitemprice = oitemprice;
        this.oitemdescp = oitemdescp;
    }



    public int getOimage() {
        return oimage;
    }

    public void setOimage(int oimage) {
        this.oimage = oimage;
    }

    public String getOitem() {
        return oitem;
    }

    public void setOitem(String oitem) {
        this.oitem = oitem;
    }

    public String getOitemdescp() {
        return oitemdescp;
    }

    public void setOitemdescp(String oitemdescp) {
        this.oitemdescp = oitemdescp;
    }



    int oimage;
    String oitem;



    String oitemprice;
    String oitemdescp;

    public String getOitemprice() {
        return oitemprice;
    }

    public void setOitemprice(String oitemprice) {
        this.oitemprice = oitemprice;
    }



}
