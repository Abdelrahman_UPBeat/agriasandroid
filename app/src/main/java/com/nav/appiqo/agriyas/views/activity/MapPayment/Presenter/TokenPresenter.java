package com.nav.appiqo.agriyas.views.activity.MapPayment.Presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.google.android.gms.common.api.Api;
import com.nav.appiqo.agriyas.SettingSaved;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiClientOnlinePayment;
import com.nav.appiqo.agriyas.WebApi.ApiClientOnlinePaymentTwo;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.CodPlaceModel;
import com.nav.appiqo.agriyas.models.AmmountMain;
import com.nav.appiqo.agriyas.models.AuthResponce;
import com.nav.appiqo.agriyas.models.GetTokenRequest;
import com.nav.appiqo.agriyas.models.orderTry;
import com.nav.appiqo.agriyas.views.activity.MapPayment.MapActivity;
import com.nav.appiqo.agriyas.views.activity.MapPayment.MapTockenContract;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import payment.sdk.android.PaymentClient;
import payment.sdk.android.cardpayment.CardPaymentRequest;
import retrofit2.Call;
import retrofit2.Callback;

public class TokenPresenter extends AsyncTask<String, String, String> implements MapTockenContract.IpaymentPresenter {

    // private final ApiInterface apiService;
    private MapTockenContract.IpaymentView iPaymentView;
    private ApiInterface apiInterface;


    public TokenPresenter(MapTockenContract.IpaymentView iPaymentView) {
        this.iPaymentView = iPaymentView;
        //  apiService = ApiClientOnlinePayment.getClient();

    }

    @Override
    public void getTockenForPayment(String client_credentials) {
        Call<AuthResponce> call = ApiClientOnlinePayment.getClient().getTockenForPayment(client_credentials);
        call.enqueue(new Callback<AuthResponce>() {
            @Override
            public void onResponse(Call<AuthResponce> call, retrofit2.Response<AuthResponce> resultResponse) {
                AuthResponce response = resultResponse.body();

                if (response != null) {
                    iPaymentView.onpaymentSuccess(response.getAccess_token());

                } else {
                    iPaymentView.onpaymentFailure("fadddy");

                }
            }

            @Override
            public void onFailure(Call<AuthResponce> call, Throwable t) {
                iPaymentView.onpaymentFailure("Errrror");
            }
        });
    }

    @Override
    public void getOrderForPayment(String action, AmmountMain amount, String outletId, String Token) {

        Call<orderTry> call = ApiClientOnlinePaymentTwo.getClient(Token).getOrderForPayment(action);//,outletId);
        call.enqueue(new Callback<orderTry>() {
            @Override
            public void onResponse(Call<orderTry> call, retrofit2.Response<orderTry> resultResponse) {
                orderTry response = resultResponse.body();

                if (response != null) {
                    iPaymentView.onOrderSuccess("asas");

                } else {
                    iPaymentView.onOrderFailure("wrong");

                }
            }

            @Override
            public void onFailure(Call<orderTry> call, Throwable t) {
                iPaymentView.onOrderFailure("Errrror");
            }
        });
    }

    @Override
    public void postOrder(String user_id, String address, String flat_no, String landmark, String item, String amount, String mobile, String lang, Context context, String PayMethod) {
        apiInterface = ApiClient.getClient(context).create(ApiInterface.class);

        Call<CodPlaceModel> call = apiInterface.codOrder(user_id, address, flat_no, landmark, item, amount, mobile, "ar", PayMethod);
        call.enqueue(new Callback<CodPlaceModel>() {
            @Override
            public void onResponse(Call<CodPlaceModel> call, retrofit2.Response<CodPlaceModel> resultResponse) {
                CodPlaceModel response = resultResponse.body();
                if (response != null) {


                    iPaymentView.onOrderPostSuccess(response);

                    iPaymentView.onOrderPostFailure(response.getMessage());


                } else {

                    iPaymentView.onOrderPostFailure("server Error");

                }
            }

            @Override
            public void onFailure(Call<CodPlaceModel> call, Throwable t) {
                if (t.getMessage() != null)
                    iPaymentView.onOrderPostFailure("server Error");
            }


        });
    }

    public void call(String Token, Activity context, Context contextw, String Price) {


        SettingSaved.Token = Token;


        int s = Integer.parseInt(Price) * 100;
        String ss = Integer.toString(s);

     //   ss = "1";


        final String[] PaymentGetway = new String[1];
        final String[] PaymentGetway1 = new String[1];
        class cls extends AsyncTask<String, String, String> {


            @Override
            protected String doInBackground(String... strings) {

//                String data = "{\"action\":" + "\"" + "SALE" + "\"," +
//                        "\"amount\" : {\"currencyCode\" :\"" + "AED" + "\",\"value\" : " + "100" + "}," +
//                        "\"language\" : \"" + "en" + "\"," +
//                        "\"description\" : \"" + "sdsd" + "\"," +
//                        "\"merchantAttributes\" : {\"redirectUrl\": \"" + "www.google.com" + "\"}}";

//new https://api-gateway.ngenius-payments.com/transactions/outlets/

                // old  https://api-gateway-uat.ngenius-payments.com/transactions/outlets/830db361-f7db-42dd-a5b0-891c83bc63cc/orders
                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/vnd.ni-payment.v2+json");
                RequestBody body = RequestBody.create(mediaType, "{\"action\":\"SALE\",\"amount\":{\"currencyCode\":\"AED\",\"value\":" + ss + "},\"emailAddress\":\"aiu@y.com\"}");
                //   RequestBody body = RequestBody.create(mediaType, data);
                Request request = new Request.Builder()
                        .url("https://api-gateway.ngenius-payments.com/transactions/outlets/aabf1794-6e55-425a-8457-fda2adc4ad90/orders")
                        .post(body)
                        .addHeader("Authorization", "bearer " + Token)
                        .addHeader("Content-Type", "application/vnd.ni-payment.v2+json")
                        .addHeader("Accept", "application/vnd.ni-payment.v2+json").
                        //     .addHeader("Content-Type", "application/json")
                        //  .addHeader("Authorization"," Basic:"+Token).
                                build();

                Response response;

                {
                    try {
                        response = client.newCall(request).execute();
                        String x = response.body().string();
                        Log.d("resssps", x);
                        //      orderTry orderTry = response.body();
                        String split[] = x.split(",");
                        //   Log.d("ressspswwww", split[5]);
                        String split2[] = split[5].split(":");
                        PaymentGetway[0] = split2[2] + ":" + split2[3];
                        PaymentGetway[0] = PaymentGetway[0].replaceAll("^\"|\"$", "");
                        //        PaymentGetway = PaymentGetway.replaceAll("}","");
                        PaymentGetway[0] = PaymentGetway[0].substring(0, PaymentGetway[0].length() - 2);
                        Log.d("ressspswwww2forCode", PaymentGetway[0]);


                        String splitPAyment[] = split[2].split(":");
                        PaymentGetway1[0] = splitPAyment[2] + ":" + splitPAyment[3];
                        PaymentGetway1[0] = PaymentGetway1[0].replaceAll("^\"|\"$", "");
                        PaymentGetway1[0] = PaymentGetway1[0].substring(0, PaymentGetway1[0].length() - 2);
                        Log.d("ressspswwww2forAuth", PaymentGetway1[0]);


//                        String url = PaymentGetway;
//                        Intent i = new Intent(Intent.ACTION_VIEW);
//                        i.setData(Uri.parse(url));
//                        context.startActivity(i);


                        Log.d("token", Token);
                        String REf = split[split.length - 1];
                        String REf2[] = REf.split(":");
                        REf2[1] = REf2[1].replaceAll("^\"|\"$", "");
                        REf2[1] = REf2[1].substring(0, REf2[1].length() - 5);


                        SettingSaved.Reffr = REf2[1];

                        //   callOrderDetails(Token,REf2[1]);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                iPaymentView.onOrderSuccess(PaymentGetway[0] + "_" + PaymentGetway1[0]);
//                Log.d("herewww",PaymentGetway[0]);
//
//                WebView mWebView = new WebView(contextw);
//                mWebView.getSettings().setJavaScriptEnabled(true);
//                mWebView.loadUrl("www.google.com");
//
//                Log.d("herewwwqqq",PaymentGetway[0]);


//
//                CardPaymentRequest.Builder b = new CardPaymentRequest.Builder();
//                CardPaymentRequest req;
//////
////        Log.d("resewwwwwpamentGetWay", x);
//                b.gatewayUrl("https://api-gateway-uat.ngenius-payments.com/transactions/paymentAuthorization");
//                b.code("6dc4d7403f6c89b3");
//                req = b.build();
////        Log.d("resepswwwww2code", spilt3[1]);
////        req = b.build();
//                PaymentClient p = new PaymentClient(context);
//                p.launchCardPayment(b.build(), 1);


            }
        }
        cls c = new cls();
        c.execute();


    }


    public void callOrderDetails(String Token, String OrderRefrence) {


        class cls extends AsyncTask<String, String, String> {


            @Override
            protected String doInBackground(String... strings) {


                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url("https://api-gateway.ngenius-payments.com/transactions/outlets/aabf1794-6e55-425a-8457-fda2adc4ad90/orders/" + OrderRefrence)
                        .get()
                        .addHeader("accept", "application/vnd.ni-payment.v2+json")
                        .addHeader("authorization", "bearer " + Token)
                        .build();

                Response response;

                {
                    try {
                        response = client.newCall(request).execute();
                        String x = response.body().string();
                        Log.d("orderDetails", x);
                        Log.d("Linkwww", "https://api-gateway.ngenius-payments.com/transactions/outlets/aabf1794-6e55-425a-8457-fda2adc4ad90/orders/" + OrderRefrence);


                        Log.d("tokenqwqw", Token);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                //   iPaymentView.onOrderSuccess();

            }
        }
        cls c = new cls();
        c.execute();


    }

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }


}
