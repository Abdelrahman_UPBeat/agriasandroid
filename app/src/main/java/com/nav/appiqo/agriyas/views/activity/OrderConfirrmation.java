package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderConfirrmation extends AppCompatActivity {

    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_order_amount)
    TextView tvOrderAmount;
    @BindView(R.id.btn_orderhistory)
    Button btnOrderhistory;
    @BindView(R.id.tv_tocart)
    TextView tvTocart;
    @BindView(R.id.cardconfirm)
    CardView cardconfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirrmation);
        ButterKnife.bind(this);

        String orderId= getIntent().getStringExtra("orderId");
        String PurchaseAmount= getIntent().getStringExtra("purchaseAmount");
        tvOrderId.setText(orderId);
        tvOrderAmount.setText(PurchaseAmount);
        MyApplication.writeStringPref(PrefData.sizeofcart,"0");

    }

    @OnClick({R.id.btn_orderhistory, R.id.tv_tocart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_orderhistory:

                startActivity(new Intent(OrderConfirrmation.this,ControllerActivity.class).putExtra("key",true)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK ));

                break;
            case R.id.tv_tocart:
                startActivity(new Intent(OrderConfirrmation.this,ControllerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK ));

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    startActivity(new Intent(OrderConfirrmation.this,ControllerActivity.class).
            setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK ));
    }
}
