package com.nav.appiqo.agriyas;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.nav.appiqo.agriyas.WebApi.CodPlaceModel;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.MapPayment.MapActivity;
import com.nav.appiqo.agriyas.views.activity.MapPayment.MapTockenContract;
import com.nav.appiqo.agriyas.views.activity.MapPayment.Presenter.TokenPresenter;
import com.nav.appiqo.agriyas.views.activity.MyCart;
import com.nav.appiqo.agriyas.views.activity.OrderConfirrmation;
import com.nav.appiqo.agriyas.views.fragment.FragmentConfimation;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import payment.sdk.android.PaymentClient;
import payment.sdk.android.cardpayment.CardPaymentData;
import payment.sdk.android.cardpayment.CardPaymentRequest;

public class TryAct extends AppCompatActivity implements MapTockenContract.IpaymentView {


    TokenPresenter tocken;
    String etFlatnumber, etArea, etLandmark, item, totalAmount, phone, Auth, AuthCode;
    private ProgressView progressView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try);


        // tokenPresenter = new TokenPresenter(this);
        progressView = new ProgressView(TryAct.this);

        try {
            Auth = getIntent().getExtras().getString("Auth");
            AuthCode = getIntent().getExtras().getString("AuthCode");

            etFlatnumber = getIntent().getExtras().getString("etFlatnumber");
            etArea = getIntent().getExtras().getString("etArea");
            etLandmark = getIntent().getExtras().getString("etLandmark");

            item = getIntent().getExtras().getString("item");
            totalAmount = getIntent().getExtras().getString("totalAmount");
            phone = getIntent().getExtras().getString("phone");



        CardPaymentRequest.Builder b = new CardPaymentRequest.Builder();
        CardPaymentRequest req;
        b.code(AuthCode);
        b.gatewayUrl(Auth);
        req = b.build();

        PaymentClient p = new PaymentClient(TryAct.this);
        p.launchCardPayment(req, 1);

        tocken = new TokenPresenter(this);
        } catch (Exception e) {
        }



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //  super.onActivityResult(requestCode,resultCode,data);
        Log.d("resultCode", Integer.toString(resultCode));
        Log.d("requestCode", Integer.toString(requestCode));


        if (requestCode == 1) {

            if (Activity.RESULT_CANCELED == resultCode) {

                Log.d("doneww", "canceled");

            } else if (Activity.RESULT_OK == resultCode) {
                CardPaymentData cardPaymentData = CardPaymentData.getFromIntent(data);
                //  Toast.makeText(getApplicationContext(), "payment done", Toast.LENGTH_LONG).show();
                Log.d("doneww", "ok");
                //   Log.d("doneww",cardPaymentData.getReason());
                //   connectApiToPlaceOrder("Online Payment");


                callOrderDetails(SettingSaved.Token, SettingSaved.Reffr);

//                try {
//                    Log.d("donewwee", Integer.toString(cardPaymentData.getCode()));
//                } catch (Exception e) {
//
//                    Log.d("donewwee", "False");
//
//                }


            } else {

                Toast.makeText(getApplicationContext(), "payment Didn't paid", Toast.LENGTH_LONG).show();
                Log.d("doneww", "not Paid");
                finish();

                //     connectApiToPlaceOrder("Online Payment");


            }

        } else {

            Log.d("doneww", "meshda5el");
        }

    }


    public void callOrderDetails(String Token, String OrderRefrence) {
        final String[] res = {""};

        class cls extends AsyncTask<String, String, String> {


            @Override
            protected String doInBackground(String... strings) {


                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url("https://api-gateway.ngenius-payments.com/transactions/outlets/aabf1794-6e55-425a-8457-fda2adc4ad90/orders/" + OrderRefrence)
                        .get()
                        .addHeader("accept", "application/vnd.ni-payment.v2+json")
                        .addHeader("authorization", "bearer " + Token)
                        .build();

                Response response;

                {
                    try {
                        response = client.newCall(request).execute();
                         res[0] = response.body().string();
                       // Log.d("orderDetails", res[0]);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                //   iPaymentView.onOrderSuccess();
                Log.d("orderDetails", res[0]);

                if (res[0].contains("CAPTURED")) {


                    Log.d("statue", "suc");
                    connectApiToPlaceOrder("Online Payment");

                } else {
                    Log.d("statue", "fail");

                   finish();

                }

            }
        }
        cls c = new cls();
        c.execute();


    }


    private void connectApiToPlaceOrder(String PayMethod) {
        progressView.showLoader();

        String address = etFlatnumber + "," + etArea + "," + etLandmark;

        tocken.postOrder(MyApplication.readStringPref(PrefData.USER_ID), address, etFlatnumber, etLandmark, item, totalAmount, phone.toString(), "ar"
                , TryAct.this, PayMethod);


    }

    @Override
    public void onpaymentFailure(String TokenError) {

    }

    @Override
    public void onpaymentSuccess(String Tokensuccess) {

    }

    @Override
    public void onOrderFailure(String TokenError) {

    }

    @Override
    public void onOrderSuccess(String x) {

    }

    @Override
    public void onOrderPostSuccess(CodPlaceModel codPlaceModel) {
        progressView.hideLoader();

        String orderId = "" + codPlaceModel.getData().getOrder_id();
        String purchaseAmount = "" + codPlaceModel.getData().getAmount();
        MyApplication.writeStringPref(PrefData.pucaseamount, purchaseAmount);
        Log.e("amount", purchaseAmount);
        // ((ControllerActivity)getActivity()).finish();
        Bundle bundle = new Bundle();
        Bundle bundlecart = new Bundle();
        bundle.putString("orderId", orderId);
        bundle.putString("purchaseAmount", purchaseAmount);
        // bundlecart.putString("purchaseAmount", purchaseAmount);
        //set Fragmentclass Arguments
        FragmentConfimation fragobj = new FragmentConfimation();
        //  Fragment_Cart fragment_cart=new Fragment_Cart();
        fragobj.setArguments(bundle);
        // fragment_cart.setArguments(bundlecart);
        startActivity(new Intent(TryAct.this, OrderConfirrmation.class).putExtra("orderId", orderId).putExtra("purchaseAmount", purchaseAmount)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onOrderPostFailure(String x) {

    }
}
