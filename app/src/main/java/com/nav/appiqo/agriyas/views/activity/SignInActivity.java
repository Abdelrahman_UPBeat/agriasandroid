package com.nav.appiqo.agriyas.views.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.Utility.Utils;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.LoginModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.helper.Validation;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    public static final String ERROR_MESSAGE = "Error";
    @BindView(R.id.emailEditText)
    TextInputEditText emailEditText;
    @BindView(R.id.emailTextInputLayout)
    TextInputLayout emailTextInputLayout;
    @BindView(R.id.passwordEditText)
    TextInputEditText passwordEditText;
    @BindView(R.id.passwordTextInputLayout)
    TextInputLayout passwordTextInputLayout;

    @BindView(R.id.tv_frtpass)
    TextView tvFrtpass;
    @BindView(R.id.tv_sinin)
    TextView tvSinin;
    @BindView(R.id.sinin)
    TextView sinin;
    Locale myLocale;

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;
    @BindView(R.id.root_login)
    LinearLayout rootLogin;
    @BindView(R.id.tv_linktosinup)
    TextView tvLinktosinup;
    private String type = "";
   /* @BindView(R.id.iv_sininbackarrow)
    ImageView ivSininbackarrow;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        progressView = new ProgressView(SignInActivity.this);
        apiInterface = ApiClient.getClient(SignInActivity.this).create(ApiInterface.class);
        prefData = new PrefData(SignInActivity.this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString(Util.LANG_KEY_SHARED, Util.ENGLISH);
        try{
            type = getIntent().getStringExtra("type");

        }catch (Exception e){
            type = "";
        }
      //  changeLocale(name);

        tvFrtpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, ForgotActivity.class);
                startActivity(intent);
            }
        });

        tvLinktosinup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                intent.putExtra("type",type);
                startActivity(intent);
            }
        });

       /* ivSininbackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
*/
        tvSinin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                if (Validation.nullValidator(username)) {
                    Utils.showSnackBar(rootLogin, getResources().getString(R.string.PleaseEnterEmailID), emailEditText, SignInActivity.this);
                } else if (!Validation.emailValidator(username)) {
                    Utils.showSnackBar(rootLogin, getResources().getString(R.string.PleaseEnterValidEmailID), emailEditText, SignInActivity.this);
                } else if (Validation.nullValidator((password))) {
                    Utils.showSnackBar(rootLogin, getResources().getString(R.string.PleaseEnterpassword), passwordEditText, SignInActivity.this);
                } else if (!Validation.passValidator((password))) {
                    Utils.showSnackBar(rootLogin, getResources().getString(R.string.PleaseEntersixdigitpassword), passwordEditText, SignInActivity.this);
                } else {
                    connectApiLogin();
                }

            }

        });
    }

    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase("")) {
            Log.e("lan", "changeLocale: not");
            return;
        }

        myLocale = new Locale(lang);
        Log.e("lan", "changeLocale1: ");//Set Selected Locale

        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//Update the config
       // updateViews();//Update texts according to locale
    }


    private void updateViews() {
        emailEditText.setText(R.string.email_id);
        passwordEditText.setText((R.string.password));
        tvFrtpass.setText(R.string.forget_password);
        tvSinin.setText(R.string.sign_in);
        sinin.setText(R.string.sign_In);
    }

    private void connectApiLogin() {
        progressView.showLoader();
        String emailAddress = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();


        Call<LoginModel> call = apiInterface.login(emailAddress, password, "Android",  MyApplication.readStringPref(PrefData.DEVICE_ID), "uiuinuuybiyvuy",Util.getLocale());
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                progressView.hideLoader();

                LoginModel body = response.body();
                if (body != null) {
                    if (!body.isError()) {
                        Log.i("tag46", "onResponse: login" + body.getData().getImage());
                        Log.i("tag46", "onResponse: device " + body.getData().getDevice_id());
                        Log.i("tag46", "onResponse: security " + body.getData().getSecurity_token());
                        Log.i("tag46", "onResponse: userid " + body.getData().getUser_id());

                        MyApplication.writeStringPref(PrefData.USER_ID, body.getData().getUser_id());
                        MyApplication.writeStringPref(PrefData.user_mobile, body.getData().getMobile());
                        MyApplication.writeStringPref(PrefData.PREF_FCM_TOK, body.getData().getSecurity_token());
                        MyApplication.writeStringPref(PrefData.PREF_DEVIC_ID, body.getData().getDevice_id());
                        MyApplication.writeStringPref(PrefData.User_userfile, body.getData().getImage());
                        MyApplication.writeStringPref(PrefData.full_name, body.getData().getFull_name());
                        MyApplication.writeStringPref(PrefData.user_email, body.getData().getEmail());
                        MyApplication.writeStringPref(PrefData.deviceId, body.getData().getDevice_id());
                        MyApplication.writeStringPref(PrefData.securityToken, body.getData().getSecurity_token());
                        MyApplication.writeBooleanPref(PrefData.LOGINSTATUS, true);


                        Intent intent = new Intent(SignInActivity.this, ControllerActivity.class);
                        intent.putExtra("guestLogin", "signIn");
                        try{
                            if (type.equals("1")){
                                intent.putExtra("type","1");
                            }
                        }catch (Exception e){}
                        startActivity(intent);
                        WalkThroughActivity.walkThroughActivity.finish();
                        finish();
                    } else {
                        Util.showSnackBar(rootLogin, body.getMessage(), SignInActivity.this);
                    }
                } else {
                    Util.showSnackBar(rootLogin, ERROR_MESSAGE, SignInActivity.this);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                progressView.hideLoader();
                if (NetworkUtil.isOnline(SignInActivity.this)) {
                    Toast.makeText(SignInActivity.this, "Something gone wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(SignInActivity.this, "Bad Network", Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}

