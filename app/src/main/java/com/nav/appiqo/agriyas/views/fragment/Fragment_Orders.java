package com.nav.appiqo.agriyas.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.MyOrdersModel;
import com.nav.appiqo.agriyas.adapter.OrderFragmentAdapter;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.nav.appiqo.agriyas.views.activity.SignInActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Orders extends Fragment {
    @BindView(R.id.iv_menutool)
    ImageView ivMenutool;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_orderfrag)
    RecyclerView rvOrderfrag;
    @BindView(R.id.myordermainlayout)
    LinearLayout myordermainlayout;

    List<MyOrdersModel.DataBean> myOrderModelListfrag;
    OrderFragmentAdapter orderFragmentAdapter;

    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order, container, false);
        unbinder = ButterKnife.bind(this, v);

        myOrderModelListfrag = new ArrayList<>();
        progressView = new ProgressView(getActivity());
        apiInterface = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiInterface.class);
        prefData = new PrefData(getActivity().getApplicationContext());

        ivMenutool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ControllerActivity) getActivity()).drawer();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvOrderfrag.setLayoutManager(linearLayoutManager);
        orderFragmentAdapter = new OrderFragmentAdapter(getActivity(), myOrderModelListfrag);
        rvOrderfrag.setAdapter(orderFragmentAdapter);


        connectApiMyOrder();

        return v;
    }


    private void connectApiMyOrder() {
        progressView.showLoader();

        String userid = MyApplication.readStringPref(PrefData.USER_ID);

        Call<MyOrdersModel> call = apiInterface.myOrder(userid,Util.getLocale());
        call.enqueue(new Callback<MyOrdersModel>() {
            @Override
            public void onResponse(Call<MyOrdersModel> call, Response<MyOrdersModel> response) {
                progressView.hideLoader();
                MyOrdersModel body = response.body();
                if(body != null) {
                    if (!body.isError()) {

                        myOrderModelListfrag.clear();
                        myOrderModelListfrag.addAll(body.getData());

                        orderFragmentAdapter.notifyDataSetChanged();


                    } else {
                        Util.showSnackBar(myordermainlayout, body.getMessage(), getActivity());

                    }
                }else{
                    Util.showSnackBar(myordermainlayout, SignInActivity.ERROR_MESSAGE, getActivity());
                }
            }

            @Override
            public void onFailure(Call<MyOrdersModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
