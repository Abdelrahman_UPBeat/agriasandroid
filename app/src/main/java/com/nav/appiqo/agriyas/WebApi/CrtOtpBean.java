package com.nav.appiqo.agriyas.WebApi;

public class CrtOtpBean {


    /**
     * error : false
     * error_code : 100
     * message : OTP sent successfully.
     * data : {}
     */

    private boolean error;
    private int error_code;
    private String message;
    private DataBean data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
    }
}
