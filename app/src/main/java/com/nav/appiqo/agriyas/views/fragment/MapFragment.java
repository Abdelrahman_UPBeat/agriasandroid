package com.nav.appiqo.agriyas.views.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.Utility.Util;
import com.nav.appiqo.agriyas.WebApi.ApiClient;
import com.nav.appiqo.agriyas.WebApi.ApiInterface;
import com.nav.appiqo.agriyas.WebApi.CodPlaceModel;
import com.nav.appiqo.agriyas.WebApi.NetworkUtil;
import com.nav.appiqo.agriyas.helper.MyApplication;
import com.nav.appiqo.agriyas.helper.PrefData;
import com.nav.appiqo.agriyas.helper.ProgressView;
import com.nav.appiqo.agriyas.views.activity.ControllerActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnCameraChangeListener {


    public static int MY_PERMISSION_FINE_LOCATION = 101;
    @BindView(R.id.toolbarmap_icon)
    ImageView toolbarmapIcon;
    /*@BindView(R.id.search_barmap)
    SearchView searchBarmap;
    @BindView(R.id.toolbarsearc)
    AppBarLayout toolbarsearc;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;*/
    @BindView(R.id.toolbarmap)
    AppBarLayout toolbarmap;
    ProgressView progressView;
    PrefData prefData;
    ApiInterface apiInterface;

    View v;
    EditText edt_flatno, edt_area, edt_landmark;
    String item = "", paymentMode = "", address = "", totalAmount = "";
    RadioButton rb;
    BottomSheetDialog mBottomSheetDialog;
    Dialog dialog1;
    Unbinder unbinder;
    GoogleApiClient mgoogleapiclient;
    private GoogleMap nmap;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mapfragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        progressView = new ProgressView(getActivity());
        apiInterface = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        prefData = new PrefData(getActivity());

        // showDialog(getActivity());

   /*     if (googleserviceavailable()){

            MapFragment nmap = (MapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
          *//*
            mgoogleapiclient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .build();
*//*
        }*/

        toolbarmapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        toolbarmap.bringToFront();
        //toolbarsearc.bringToFront();

        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            item = bundle.getString("jsaonData");
            totalAmount = bundle.getString("finalAmountOfOrder");

        }


        //   nmap.getMapAsync(this);
         /*   mgoogleapiclient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)

                    .build();
*/
        // searchBarmap = v.findViewById(R.id.search_barmap);
        // ImageView searchIcon = searchBarmap.findViewById(android.support.v7.appcompat.R.id.search_button);
        // searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_search));

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    public void onMapReady(GoogleMap googleMap) {
        nmap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            nmap.setMyLocationEnabled(true);
            nmap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);

            }
        }


    }


    public void paymentdialog(Context con) {
        dialog1.dismiss();

        mBottomSheetDialog = new BottomSheetDialog(con);
        final View sheetView = getActivity().getLayoutInflater().inflate(R.layout.paymentdialog, null);
        mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

        final RadioButton rb1, rb3;
        RadioGroup rgPaymentMode;

        TextView Tv_cancel, Tv_OK;
        rb1 = sheetView.findViewById(R.id.rb_payment);
        rb3 = sheetView.findViewById(R.id.rb_payment3);
        rgPaymentMode = sheetView.findViewById(R.id.rg_payment_mode);
        Tv_cancel = sheetView.findViewById(R.id.tv_cancel);
        Tv_OK = sheetView.findViewById(R.id.tv_ok);

        rgPaymentMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton) group.findViewById(checkedId);

                switch (checkedId) {
                    case R.id.rb_payment:
                        paymentMode = rb1.getText().toString();
                        Log.e("hello", "card");
                        break;
                    case R.id.rb_payment3:
                        paymentMode = rb3.getText().toString();
                        Log.e("hello", "cod");
                        break;
                }
            }
        });

        Tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        Tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentMode.equalsIgnoreCase("Credit Card")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.currentlynotavailable), Toast.LENGTH_SHORT).show();
                } else if (paymentMode.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.paymentmethod), Toast.LENGTH_SHORT).show();
                } else if (paymentMode.equalsIgnoreCase("Cash On delivery")) {
                    connectApiToPlaceOrder();
                }
            }
        });


        mBottomSheetDialog.show();
    }

    private void connectApiToPlaceOrder() {
        address = edt_flatno.getText().toString() + "," + edt_area.getText().toString() + "," + edt_landmark.getText().toString();

        progressView.showLoader();
        Call<CodPlaceModel> call = apiInterface.codOrder(
                MyApplication.readStringPref(PrefData.USER_ID),
                address,
                edt_flatno.getText().toString(),
                edt_landmark.getText().toString(),
                item,
                totalAmount,"6789", Util.getLocale(),"1");
        call.enqueue(new Callback<CodPlaceModel>() {
            @Override
            public void onResponse(Call<CodPlaceModel> call, Response<CodPlaceModel> response) {
                progressView.hideLoader();
                if (!response.body().isError()) {

                    mBottomSheetDialog.dismiss();

                    String orderId = "" + response.body().getData().getOrder_id();
                    String purchaseAmount = "" + response.body().getData().getAmount();
                    MyApplication.writeStringPref(PrefData.pucaseamount, purchaseAmount);
                    Log.e("amount", purchaseAmount);
                    // ((ControllerActivity)getActivity()).finish();
                    Bundle bundle = new Bundle();
                    Bundle bundlecart = new Bundle();
                    bundle.putString("orderId", orderId);
                    bundle.putString("purchaseAmount", purchaseAmount);
                    // bundlecart.putString("purchaseAmount", purchaseAmount);
                    //set Fragmentclass Arguments
                    FragmentConfimation fragobj = new FragmentConfimation();
                    //  Fragment_Cart fragment_cart=new Fragment_Cart();
                    fragobj.setArguments(bundle);
                    // fragment_cart.setArguments(bundlecart);
                    //startActivity(new Intent(getActivity(), FragmentConfimation.class).putExtra("orderId", orderId).putExtra("purchaseAmount", purchaseAmount));
                    ((ControllerActivity) getActivity()).displayCaned(fragobj, "confirm");
                    //   ((ControllerActivity) getActivity()).displayCaned(fragment_cart,"confirm");
                } else {
                    mBottomSheetDialog.dismiss();
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CodPlaceModel> call, Throwable t) {
                t.printStackTrace();
                progressView.hideLoader();
                mBottomSheetDialog.dismiss();

                if (NetworkUtil.isOnline(getActivity())) {
                    Toast.makeText(getActivity(), "Something gone wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), "Bad Network", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}