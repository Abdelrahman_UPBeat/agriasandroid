package com.nav.appiqo.agriyas.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AmmountMain {

    public Ammount getAmmount() {
        return ammount;
    }

    public void setAmmount(Ammount ammount) {
        this.ammount = ammount;
    }

    @SerializedName("amount")
    @Expose
    private Ammount ammount;
}
