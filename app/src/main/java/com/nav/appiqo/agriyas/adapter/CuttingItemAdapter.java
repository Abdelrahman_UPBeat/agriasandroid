package com.nav.appiqo.agriyas.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import com.nav.appiqo.agriyas.R;
import com.nav.appiqo.agriyas.WebApi.CuttingTypeModel;
import com.nav.appiqo.agriyas.views.fragment.ItemDescriptionFragment;

import java.util.ArrayList;
import java.util.List;

public class CuttingItemAdapter extends RecyclerView.Adapter<CuttingItemAdapter.MyViewHolder>  {
    public static List<CuttingTypeModel.DataBeanCutting> cuttings;
    Context context;
    int pos = -1;
    public static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public CuttingItemAdapter(Context context, List<CuttingTypeModel.DataBeanCutting> cuttings) {
        this.cuttings = cuttings;
        this.context = context;
        sharedPreferences = context.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Override
    public CuttingItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cutting, parent, false);

        return new CuttingItemAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CuttingItemAdapter.MyViewHolder holder, int position) {
        /*LibraryDanceStarPojo danceStarPojo = arraylist_folder_all.get(position);*/
        if (sharedPreferences.getString(Locale_KeyValue, "").equals("en")){
            holder.cuttingType.setText(cuttings.get(position).getType());

        }else {
            try{
                holder.cuttingType.setText(cuttings.get(position).getType_ar());

            }catch (Exception e){
                holder.cuttingType.setText(cuttings.get(position).getType());

            }

        }
        if (pos==position){
            holder.cuttingImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_radio_button_checked_black_24dp));
        }else {
            holder.cuttingImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp));

        }
    }

    @Override
    public int getItemCount() {
        return cuttings.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView cuttingType;
        public ImageView cuttingImage;

        public MyViewHolder(View view) {
            super(view);
            cuttingType = (TextView) view.findViewById(R.id.rv_Cutting);
            cuttingImage = (ImageView) view.findViewById(R.id.cuttingImage);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            pos = getPosition();
            ItemDescriptionFragment.typeOfCutting = cuttings.get(getPosition()).getType();

            notifyDataSetChanged();
        }
    }

}

